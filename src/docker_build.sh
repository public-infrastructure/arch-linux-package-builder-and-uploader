# This script builds a docker image and uploads it to a container registry

#
# Define some defaults
#

# Enforce that we're on a tagged branch
enforce_tagged_commit="YES"

# Upload to container registry
upload_to_registry="YES"

# Tag the image as the latest
tag_as_latest="YES"

# the branch we'll be building the container image from. This will default to the tag name
branch=""

# The tag we're using. Will be populated later
tag=""

# The url and port of the private repo. Will be populated if needed
private_repo=""

# The name of the repo. Will populate later
repo_name=""

# The username of the repo. Will populate later
username=""

# The secret to login to docker. Will populate later
DOCKER_PASSWORD=""

#
## Argument Parsing
#

function print_help() {
    echo
    echo "-c {branch_name} | --no-tagged-commit {branch_name}"
    echo "Don't enforce that we're on a tagged branch."
    echo "Provide the branch that we'd like to use to build."
    echo
    echo "-t {tag_name} | --tag {tag_name}"
    echo "Provide the tag that we'd like to use to tag the container image."
    echo "If we're enforcing a tagged commit this option is ignored. Gitlabs tag name will be used in this case."
    echo "If we're not enforcing a tagged commit this will default to the branch name but can be overriden with this option"
    echo "Note that branch name will still be used to build the image but the container will be tagged with \`tag_name\`"
    echo
    echo "-r | --no-registry-upload"
    echo "Don't upload to a registry."
    echo
    echo "-p {url} | --private-repo {url}"
    echo "The URL of the private repo to upload to."
    echo
    echo "-l | --no-tag-latest"
    echo "Don't tag the image as the latest."
    echo
    echo "-u {username} | --username {username}"
    echo "The username of the repo."
    echo
    echo "-n {repo-name} | --repo-name {repo-name}"
    echo "The name of the repository to be uploaded to."
    echo
    echo "--secret {secret}"
    echo "The docker secret used to login to the registry"
    echo
    echo "-h | --help"
    echo "Show this help message"
    echo
}

# Parse some arguments
while [[ $# -gt 0 ]]; do
  case $1 in
    -c|--no-tagged-commit)
        enforce_tagged_commit="NO"
        branch="${2}"
        shift # past argument
        shift # past value
        ;;
    -t|--tag)
        tag="${2}"
        shift # past argument
        shift # past value
        ;;
    -r|--no-registry-upload)
        upload_to_registry="NO"
        shift # past argument
        ;;
    -l|--no-tag-latest)
        tag_as_latest="NO"
        shift
        ;;
    -p|--private-repo)
        private_repo="${2}"
        shift # past argument
        shift # past value
        ;;
    -u|--username)
        username="${2}"
        shift # past argument
        shift # past value
        ;;
    -n|--repo-name)
        repo_name="${2}"
        shift # past argument
        shift # past value
        ;;
    --secret)
	DOCKER_PASSWORD="${2}"
	shift # past argument
	shift # past value
	;;
    -h|--help)
        print_help
        exit 0
        ;;
    -*|--*)
        echo "Unknown option $1"
        exit 1
        ;;
  esac
done

#
## Helper functions
#

function enforce_tag() {
    # Check if we're on a tagged commit
    current_commit_id=$(git rev-parse HEAD)
    tagged_commit=false
    tags=""
    tag_name=""
    for i in $(git tag); do
    	tag=$(git rev-list -n 1 "${i}")
    	tags="${tags}
    ${i}    ::::    ${tag}"
    	if [ "$tag" == "$current_commit_id" ]; then
    		tagged_commit=true
		tag_name="${i}"
    	fi
    done
    
    if ! $tagged_commit ; then
    	echo
    	echo "WARNING: There are no tags associated with the current commit hash"
    	echo "We will not be uploading this build so we will not build it..."
    	echo
    	echo "Tags and their commit ID's:"
    	echo "${tags}"
    	echo
    	echo "Current commit:   ${current_commit_id}"
    	exit 0
    fi

    tag="${tag_name}"
}

function upload_image() {
    if [[ -z "${DOCKER_PASSWORD}" ]]; then
        echo "ERROR: No DOCKER_PASSWORD provided..."
        exit 2
    fi

    echo "${DOCKER_PASSWORD}" | docker login --username "${username}" --password-stdin
    if [[ -z "${private_repo}" ]]; then
	image_name="${username}/${repo_name}"
    	echo "Pushing ${image_name} to docker.io"
        docker image push --all-tags "${image_name}"
    else
    	image_name="${private_repo}/${username}/${repo_name}"
    	echo "Pushing ${image_name} to private repo ${private_repo}"
        docker image push --all-tags "${image_name}"
    fi
}


#
## Core logic to build the images
#

if [[ -z "${username}" ]]; then
    echo "ERROR: No username provided"
    exit 1
fi
if [[ -z "${repo_name}" ]]; then
    echo "ERROR: No repo-name provided"
    exit 1
fi


# Get our tag name from the tagged commit or the branch name
if [ "${enforce_tagged_commit}" == "YES" ]; then
    enforce_tag
    branch="${tag}"
else
    if [[ -z "${tag}" ]]; then
        tag="${branch}"
    fi
fi

# Make sure we have a tag
if [[ -z "${tag}" ]]; then
    echo "INTERNAL ERROR: We should never get to this point without a tag name being defined..."
    exit 1
fi

docker buildx build --platform linux/amd64 -t "${username}/${repo_name}:${tag}" --build-arg VERSION=${branch} .

if [ "${tag_as_latest}" == "YES" ]; then
    image_id=$(docker images -q "${username}/${repo_name}")
    if [[ -z "${image_id}" ]]; then
        echo "INTERNAL ERROR: Couldn't get image ID"
        exit 1
    fi
    docker image tag "${image_id}" "${username}/${repo_name}:latest"
fi

if [ "${upload_to_registry}" == "YES" ]; then
    upload_image
fi

