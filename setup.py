from setuptools import setup, find_packages  # type: ignore[import-untyped]

setup(
    name="builder",
    version="1.0.0",  # Please check with your package manager. This tag will not be maintained.
    packages=find_packages(),
    install_requires=[
        "requests",
    ],
    entry_points={
        "console_scripts": [
            "builder=builder.main:main",
        ],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: GPLV3",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
)
