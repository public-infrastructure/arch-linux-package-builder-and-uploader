# Arch Linux Package Builder and Uploader

## Getting started

This repo contains the program that MutableMango uses to automatically build a package in GitLab CI and upload that to our mirror. It can also build Docker Images and upload them to a registry. There are a few external dependencies on this package to run successfully in CI:

- A deployed instance of the Arch Linux Package Uploader.
  - https://gitlab.com/public-infrastructure/uploader
- Some web server serving the directory structure that Pacman can read.
- Docker registry (DockerHub is fine)

## Building an Arch Linux Package

```
before_script:
  # Install the requirements for this project
  - chown -R nobody:nobody .
  - echo "[mutablemango]" >> /etc/pacman.conf
  - echo "SigLevel = Optional DatabaseOptional" >> /etc/pacman.conf
  - echo "Server = https://mirror.mutablemango.com/mutablemango/archlinux/packages/" >> /etc/pacman.conf
  - /usr/bin/pacman -Syy python-black flake8 mypy python-parameterized python-setuptools expat python python-requests git python-docker python-beautifulsoup4 --noconfirm lib-mutablemango --needed
  - git config --global --add safe.directory /builds/public-infrastructure/mutablemango-build-automation

deploy:
  stage: deploy
  image: archlinux:base-devel
  script:
  # In this example, $UPLOAD_SECRET is a CI variable configured in the UI
    - sudo -u nobody builder arch_package --secret "$UPLOAD_SECRET" --upload-url "https://uploader.domain.com/upload" --package-name "builder" --base-mirror-url "https://mirror.domain.com/"
```

A few required parameters need to be passed to the script:

- --secret
  - The secret needed by the Arch Linux Package Uploader to authenticate that you are who you claim to be
- --upload-url
  - The URL that the deployed instance of the Arch Linux Package Uploader is deployed at
- --package-name
  - The name of the package. For `bash-utilities-v0.0.5-1-any.pkg.tar.zst` it's `bash-utilities`.
- --base-mirror-url
  - The URL of the mirror that we look at for current packages. In the above CI example it's `https://mirror.domain.com/`

Some Optional parameters are:
-- sign {base64 encoded string} - The base64 encoded PGP key

## Building a Docker Image

This program can build an image as well as upload it. It supports custom tags as well as custom build args

Usage of docker:

```
  -build-arg value
    	Specify build arguments (format: KEY=VALUE, can be used multiple times)
  -log-level string
    	The logging level (notset, debug, info, warning, error, critical) (default "info")
  -no-tagged-commit
    	Allow builds from non-tagged commits
  -no-upload
    	Don't upload to a registry
  -repo-name string
    	The name of the repository to use for the build. This is usually <username>/<project>
  -repo-url string
    	The name of the repository to upload to
  -secret string
    	Docker secret used to login to the registry
  -tag value
    	Specify tags (can be used multiple times)
  -username string
    	Username for the Docker repository
```

Example usage:

`builder docker --no-upload --no-tagged-commit --tag main --tag coolest --repo-name mutablemango/archlinuxpackageuploader --build-arg VERSION=main`
