import subprocess
from dataclasses import dataclass
import logging

# Get our logger
logger = logging.getLogger(__name__)


@dataclass
class CommandOutput:
    code: int
    stdout: str
    stderr: str


def run_shell_command(command):
    """
    Runs a shell command provided as an array of strings.
    """

    try:
        result = subprocess.run(command, check=True, text=True, capture_output=True)
    except subprocess.CalledProcessError as e:
        # Print stderr in the except block
        logger.info(f"Error occurred while running command: {e.stderr}")
        logger.info(e)
        raise e

    return CommandOutput(
        code=result.returncode,
        stdout=result.stdout,
        stderr=result.stderr,
    )
