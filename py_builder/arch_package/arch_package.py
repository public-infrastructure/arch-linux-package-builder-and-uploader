from builder.command import run_shell_command
import re
import os
from typing import List, Optional
import requests  # type: ignore[import-untyped]
from dataclasses import dataclass
import logging
import base64
from builder.util import is_on_tagged_commit
from bs4 import BeautifulSoup  # type: ignore[import-untyped]

# Get our logger
logger = logging.getLogger(__name__)


def create_gpg_key(signing_secret: str):
    """
    Imports a GPG key from the given secret.
    """
    logger.info("Setting up the signing secret...")

    # Decode the base64 signing secret and write it to a temporary file
    decoded_secret = base64.b64decode(signing_secret)
    with open("/tmp/private.pgp", "wb") as f:
        f.write(decoded_secret)

    # Import the GPG key
    try:
        run_shell_command(["gpg", "--import", "/tmp/private.pgp"])
    except Exception as e:
        # Clean up the temporary file
        if os.path.exists("/tmp/private.pgp"):
            os.remove("/tmp/private.pgp")

        raise e

    # Clean up the temporary file
    if os.path.exists("/tmp/private.pgp"):
        os.remove("/tmp/private.pgp")


class UpdateError(Exception):
    """Custom exception for errors during the update process."""

    pass


def update_pkgver(file_path: str, tag: str):
    logger.info(f"Updating our package version to {tag} in the file {file_path}")
    # Read the contents of the file
    with open(file_path, "r") as file:
        contents = file.read()

    # Check if the line exists
    if "pkgver=" not in contents:
        raise UpdateError("The line containing 'pkgver=' was not found in the file.")

    # Replace the target string using regex
    new_contents, num_replacements = re.subn(r"pkgver=.*", f"pkgver={tag}", contents)

    # Check if any replacements were made
    if num_replacements == 0:
        raise UpdateError("Failed to update 'pkgver=' line.")

    # Write the modified contents back to the file
    with open(file_path, "w") as file:
        file.write(new_contents)


@dataclass
class PackageFiles:
    package: str
    signature: Optional[str] = None


def get_built_files(
    before_files: List[str], signed: bool, package_name: str
) -> PackageFiles:
    logger.info("Detecting files that were built...")
    after_files = os.listdir(".")

    new_files = list(set(after_files) - set(before_files))

    filtered_files = []

    for filename in new_files:
        if package_name in filename:
            if ".pkg" in filename or ".sig" in filename:
                filtered_files.append(filename)
            else:
                logger.info(f"Discarding {filename} since it's not a .pkg or .sig")
        else:
            logger.info(
                f"Discarding {filename} since it doesn't include the package name {package_name}"
            )

    logger.info("The files that remain in our list of files are:")
    logger.info(filtered_files)

    if signed:
        if len(filtered_files) > 2:
            logger.error(filtered_files)
            raise ValueError(f"We have too many files... {len(filtered_files)}")
    else:
        if len(filtered_files) > 1:
            logger.error(filtered_files)
            raise ValueError(f"We have too many files... {len(filtered_files)}")

    # Process the files based on their content
    signature = None
    package = None
    for file in filtered_files:
        if "pkg" in file and package_name in file:
            if "sig" in file:
                signature = file
            else:
                package = file

    if package is None:
        raise ValueError("Failed to detect built package")

    if signed and signature is None:
        raise ValueError("Failed to detect build package signature")

    return PackageFiles(
        package=package,
        signature=signature,
    )


def upload_package(
    signature_path: str,
    package_path: str,
    signed: bool,
    package_name: str,
    upload_secret: str,
    upload_url: str,
):
    logger.info("Uploading our file...")
    headers = {
        "X-MutableMango-Upload-Token": upload_secret,
        "project": package_name,
    }

    files = []

    package_file = open(package_path, "rb")
    files.append(("package", package_file))

    if signature_path is not None:
        signature_file = open(signature_path, "rb")
        files.append(("signature", signature_file))

    # Upload the package file
    package_url = f"{upload_url}/package/arch"
    try:
        response = requests.post(package_url, headers=headers, data={}, files=files)
        if response.status_code != 200:
            logger.error(
                f"HTTP status code for uploading package is {response.status_code}"
            )
            response.raise_for_status()  # Raise an exception for HTTP error responses
    finally:
        package_file.close()
        if signature_path is not None:
            signature_file.close()


def extract_version(filename):
    # Regular expression pattern to match version information
    version_pattern = re.compile(r"v\d+\.\d+\.\d+")

    # Search for the pattern in the filename
    match = version_pattern.search(filename)

    # Return the matched version if found, otherwise None
    return match.group() if match else None


def get_remote_packages(base_mirror_url: str, package_name: str) -> List[str]:
    # Fetch the HTML content
    response = requests.get(base_mirror_url)
    response.raise_for_status()  # Raise an error if the request was unsuccessful

    # Parse the HTML content
    soup = BeautifulSoup(response.content, "html.parser")

    # Extract all href values in <a> tags and filter for the desired files
    files = []
    for link in soup.find_all("a"):
        href = link.get("href")
        if href and href.endswith(".pkg.tar.zst"):
            if package_name in href:
                files.append(href)

    return files


def get_sorted_versions(packages: List[str]) -> List[str]:
    versions = []
    for package in packages:
        version = extract_version(package)
        if version is not None:
            versions.append(extract_version(package))

    return sorted(set(versions), reverse=True)


def get_remote_versions(packages: List[str]) -> List[str]:
    versions = []
    for package in packages:
        version = extract_version(package)
        if version is not None:
            versions.append(extract_version(package))
    return versions


def verify_remote_package(base_mirror_url: str, package_name: str, tag: str):
    # Fetch list of packages from the remote repository
    packages = get_remote_packages(
        base_mirror_url=base_mirror_url, package_name=package_name
    )

    # If there are no remote packages then we're guaranteed to build the latest version
    if len(packages) < 1:
        return

    # Get a list of versions
    versions = get_remote_versions(packages=packages)

    # Sort the versions
    sorted_tags = get_sorted_versions(packages=packages + [tag])

    # Get the latest tag
    latest_tag = sorted_tags[0]

    # If the local tag is already uploaded
    # OR if the local tag isn't the latest
    # Don't upload
    if tag != latest_tag or (len(versions) > 0 and tag in versions):
        error_str = f"""
Current tag is not newer than deployed tag...

We do NOT support automatically building and deploying silent patches!

All new builds MUST be a newer version than what's on the mirror.
We also don't support -X releases. For example v0.3.0-1 to v0.3.0-2
Increment it to v0.3.1

Debug info:

local git tag: {tag}
latest remote tag: {latest_tag}

raw unsorted versions:
{versions}

sorting them:
{sorted_tags}

head:
{sorted_tags[0]}
        """
        logger.info(error_str)
        raise Exception("Cannot build due to version issue. See prior output")


def build_arch_package(args):
    # Use the provided branch if we shouldn't use the current tag and enforce it
    if args.no_tagged_commit:
        branch = args.branch
        tag = args.branch
    else:
        branch = is_on_tagged_commit()
        tag = branch

    # If we're uploading we cannot upload a package that is older or equal to the version on the remote mirror
    if args.no_upload is False:
        verify_remote_package(
            base_mirror_url=args.base_mirror_url,
            package_name=args.package_name,
            tag=tag,
        )

    # Import our GPG key if we're supposed to sign the package
    if args.sign:
        create_gpg_key(args.gpg_key)

    # Update the version in the package build so we can get the right one in the name
    if not args.no_update_pkgbuild_version:
        update_pkgver("PKGBUILD", tag)

    # Get the files in our directory BEFORE we build our package
    before_files = os.listdir(".")

    # Make our package
    makepkg_command = ["/usr/bin/makepkg"]

    # Sign our package
    if args.sign:
        makepkg_command.append("--sign")

    # Build our package
    logger.info("Building the package with the following command:")
    logger.info(makepkg_command)
    output = run_shell_command(makepkg_command)

    logger.info("command stdout:")
    logger.info(output.stdout)
    logger.info("command stderr:")
    logger.info(output.stderr)

    # Get our built filenames
    if args.no_upload is False or args.test_build is False:
        after_files = get_built_files(
            before_files=before_files, signed=args.sign, package_name=args.package_name
        )

    # Upload our file
    if args.no_upload is False or args.test_build is False:
        upload_package(
            built_files=after_files,
            signed=args.sign,
            package_name=args.package_name,
            upload_secret=args.secret,
            upload_url=args.upload_url,
        )
