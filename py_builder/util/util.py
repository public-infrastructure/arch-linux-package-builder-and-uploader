from builder.command import run_shell_command
import logging

# Get our logger
logger = logging.getLogger(__name__)


class NoTaggedCommitFound(Exception):
    pass


def is_on_tagged_commit():
    logger.info("Enforcing that we are on a tagged branch...")
    current_commit_id = run_shell_command(["git", "rev-parse", "HEAD"]).stdout.strip()
    tagged_commit = False
    tags = ""
    tag = ""

    git_tags = run_shell_command(["git", "tag"]).stdout.splitlines()
    for i in git_tags:
        tag_commit_id = run_shell_command(
            ["git", "rev-list", "-n", "1", i]
        ).stdout.strip()
        tags += f"\n{i}    ::::    {tag_commit_id}"
        if tag_commit_id == current_commit_id:
            tagged_commit = True
            tag = i

    if not tagged_commit:
        logger.info(
            f"\nThere are no tags associated with the current commit hash.\nWe will not be uploading this build so we will not build it...\nTags and their commit IDs:\n\nCurrent commit:   {current_commit_id}\n"
        )
        logger.info(
            "No tag found. If you're not building on a tagged commit you MUST specify --no-tagged-commit --branch {BRANCH_NAME}"
        )
        raise NoTaggedCommitFound()
    else:
        logger.info(f"Tagged commit: {tag}")
        return tag
