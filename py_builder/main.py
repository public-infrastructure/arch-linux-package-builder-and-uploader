from builder.config import get_arguments, Command
from builder.arch_package import build_arch_package
from builder.docker import build_docker
import logging
import sys
from typing import Optional


def configure_logger(level: Optional[str]):
    # Figure out our log level
    if level is None:
        log_level = logging.INFO
    elif level == "notset":
        log_level = logging.NOTSET
    elif level == "debug":
        log_level = logging.DEBUG
    elif level == "info":
        log_level = logging.INFO
    elif level == "warning":
        log_level = logging.WARNING
    elif level == "error":
        log_level = logging.ERROR
    elif level == "critical":
        log_level = logging.CRITICAL
    else:
        log_level = logging.INFO

    # Create a StreamHandler for logging to stdout
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setLevel(log_level)

    # Create a formatter and set it for the handler
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    formatter = logging.Formatter("%(message)s")
    stream_handler.setFormatter(formatter)

    # Get the root logger and set its level
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    # Add the handler to the root logger
    logger.addHandler(stream_handler)


def main():
    # Get our arguments
    args = get_arguments()

    # Setup our logging
    configure_logger(args.log_level)
    logger = logging.getLogger(__name__)

    # Run our command
    if args.command == Command.ARCH.value:
        logger.info(f"Running {args.command}")
        build_arch_package(args)
    elif args.command == Command.DOCKER.value:
        logger.info(f"Running {args.command}")
        build_docker(args)
    else:
        raise Exception("subcommand is not implemented")


if __name__ == "__main__":
    main()
