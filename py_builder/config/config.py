import argparse
from enum import Enum
from .arch_build import add_arch_build_arguments, verify_arch_build_arguments
from .docker import add_docker_arguments, verify_docker_arguments


class Command(Enum):
    ARCH = "arch_package"
    DOCKER = "docker"


def get_arguments():
    # Create our argument parser
    parser = argparse.ArgumentParser(description="MutableMango Build Automation")

    # Allow us to have subcommands
    subparsers = parser.add_subparsers(dest="command")

    # Subcommand: arch_package
    # NOTE use , aliases=['arch'] if you want to add an alias
    arch_package_parser = subparsers.add_parser(
        "arch_package", help="Arch Linux Package Build Script"
    )
    add_arch_build_arguments(arch_package_parser)

    # Subcommand: docker
    docker_parser = subparsers.add_parser("docker", help="Build Docker Containers")
    add_docker_arguments(docker_parser)

    # Parse our arguments
    args = parser.parse_args()

    # Some of these arguments require other arguments. Verify them now
    if args.command == Command.ARCH.value:
        return verify_arch_build_arguments(args, parser)
    elif args.command == Command.DOCKER.value:
        return verify_docker_arguments(args, parser)
    else:
        raise ValueError(f"{args.command} is unimplemented...")

    return args
