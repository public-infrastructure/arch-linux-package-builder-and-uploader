import argparse
import logging

# Get our logger
logger = logging.getLogger(__name__)


def verify_docker_arguments(
    arguments: argparse.Namespace, parser: argparse.ArgumentParser
):
    if arguments.no_upload is False:
        if arguments.username is None:
            parser.error("--username is required if we're uploading")
        if arguments.secret is None:
            parser.error("--secret is required if we're uploading")
        if arguments.repo_name is None and arguments.private_repo is None:
            parser.error("--private-repo OR --repo-name is required when uploading")

    return arguments


def add_docker_arguments(arch_package_parser: argparse.ArgumentParser):
    arch_package_parser.add_argument(
        "--log-level",
        type=str,
        choices=["notset", "debug", "info", "warning", "error", "critical"],
        default="info",
        help="The logging level",
    )

    arch_package_parser.add_argument(
        "--no-tagged-commit",
        action="store_true",
        help="Don't enforce that we're on a tagged branch.\nProvide the branch that we'd like to use to build instead of the tag.",
    )
    arch_package_parser.add_argument(
        "--tag",
        type=str,
        required=True,
        help="The tag to be used to tag the docker container",
    )
    arch_package_parser.add_argument(
        "--no-tag-latest",
        action="store_true",
        default=False,
        help="Do not tag image as the latest",
    )

    arch_package_parser.add_argument(
        "--secret",
        type=str,
        help="The docker secret used to login to the registry",
    )
    arch_package_parser.add_argument(
        "--no-upload",
        action="store_true",
        default=False,
        help="Don't upload to our repository",
    )
    arch_package_parser.add_argument(
        "--username",
        type=str,
        help="The username of the repo",
    )

    arch_package_parser.add_argument(
        "--repo-name",
        type=str,
        help="The name of the repository to be uploaded to.",
    )
