import argparse
import logging

# Get our logger
logger = logging.getLogger(__name__)


def verify_arch_build_arguments(
    arguments: argparse.Namespace, parser: argparse.ArgumentParser
):
    # Validate that --secret is provided if --sign is used
    if arguments.sign and not arguments.gpg_key:
        parser.error("--sign requires --gpg-key to be specified.")

    # Validate that if --branch is used, no tagged commit OR test build is specified
    # This won't break anything if we let it through but may cause confusion when it's ignored
    if (
        arguments.no_tagged_commit is False
        and arguments.test_build is False
        and arguments.branch is not None
    ):
        parser.error(
            "--branch requires --no-tagged-commit or --test-build to be specified."
        )

    # Validate that if --test-build is provided that --branch is also provided
    if arguments.test_build and not arguments.branch:
        parser.error("--test-build requires --branch to be specified.")

    if arguments.test_build and arguments.no_upload is False:
        logger.info(
            "Setting --no-upload to True since we were called with --test-build"
        )
        arguments.no_upload = True

    if arguments.test_build and arguments.no_tagged_commit is False:
        logger.info(
            "Setting --no-tagged-commit to True since we were called with --test-build"
        )
        arguments.no_tagged_commit = True

    if arguments.no_upload is False:
        if arguments.upload_url is None:
            parser.error("--upload-url is required to upload a package.")
        if arguments.package_name is None:
            parser.error("--package-name is required to upload a package.")
        if arguments.base_mirror_url is None:
            parser.error("--base-mirror-url is required to upload a package.")
        if arguments.secret is None:
            parser.error("--secret is required to upload a package.")
    return arguments


def add_arch_build_arguments(arch_package_parser: argparse.ArgumentParser):
    arch_package_parser.add_argument(
        "--log-level",
        type=str,
        choices=["notset", "debug", "info", "warning", "error", "critical"],
        default="info",
        help="The logging level",
    )

    arch_package_parser.add_argument(
        "--secret",
        type=str,
        help="The secret needed by the Arch Linux Package Uploader to authenticate that you are who you claim to be. This secret should be base64 encoded.",
    )

    arch_package_parser.add_argument(
        "--gpg-key",
        type=str,
        help="A base64 encoded GPG key that we will import into the system keyring and use to sign packages.",
    )

    arch_package_parser.add_argument(
        "--sign",
        action="store_true",
        default=False,
        help="Tell makepkg to sign the package it's creating and to upload the signature. (requires --secret)",
    )

    arch_package_parser.add_argument(
        "--no-upload",
        action="store_true",
        default=False,
        help="Don't upload to our repository",
    )

    arch_package_parser.add_argument(
        "--upload-url",
        type=str,
        help="The URL that the deployed instance of the Arch Linux Package Uploader is deployed at",
    )

    arch_package_parser.add_argument(
        "--package-name",
        type=str,
        help="The name of the package. For bash-utilities-v0.0.5-1-any.pkg.tar.zst it's bash-utilities. The - is automatically added.",
    )

    arch_package_parser.add_argument(
        "--upload-secret",
        type=str,
        help="The secret required to authenticate with the Arch Linux Package Uploader",
    )

    arch_package_parser.add_argument(
        "--base-mirror-url",
        type=str,
        help="The URL of the mirror that we look at for current packages. In the above CI example it's https://mirror.domain.com/",
    )

    arch_package_parser.add_argument(
        "--no-tagged-commit",
        action="store_true",
        help="Don't enforce that we're on a tagged branch.\nProvide the branch that we'd like to use to build instead of the tag.",
    )

    arch_package_parser.add_argument(
        "--branch", type=str, help="The branch we're supposed to build with."
    )

    arch_package_parser.add_argument(
        "--test-build",
        action="store_true",
        help="Don't upload to our mirror. Requires --branch.",
    )

    arch_package_parser.add_argument(
        "--no-update-pkgbuild-version",
        action="store_true",
        help="Do not override the version in the pkgbuild.\nThis option should be used if we're not dynamically building off of tags.\nBasically if we're hosting an AUR or official build.",
    )
