import logging
import docker  # type: ignore[import-untyped]
from docker.errors import DockerException, BuildError  # type: ignore[import-untyped]
from builder.util import is_on_tagged_commit

# Get our logger
logger = logging.getLogger(__name__)


def upload_image(username: str, image_name: str, secret: str, registry: str):
    client = docker.from_env()

    try:

        # Login to Docker
        client.login(username=username, password=secret, registry=registry)

        # Push the image
        response = client.images.push(image_name, stream=True, decode=True)
        logger.info(response)

    except DockerException as e:
        logger.error(e)
        raise e
    finally:
        client.close()


def tag_image(image: docker.models.images.Image, repository: str, new_tag: str):
    try:
        # Initialize the Docker client
        # client = docker.from_env()

        # Get the image to tag
        # image = client.images.get(image_name)

        # Tag the image
        image.tag(repository=repository, tag=new_tag)

        logger.info(f"Image {image.id} tagged as {repository}:{new_tag}")

    except docker.errors.ImageNotFound:
        logger.error(f"Image {image.id} not found.")
    except docker.errors.APIError as e:
        logger.error(f"Error occurred: {e}")
    except Exception as e:
        logger.error(f"Unexpected error: {e}")


def build_image(
    tag: str, no_tag_latest: bool, repository: str, dockerfile_path: str = "."
):
    client = docker.from_env()

    try:
        logger.info(f"Building image with tag: {tag}")
        image, logs = client.images.build(path=dockerfile_path, tag=tag)

        for log in logs:
            if "stream" in log:
                print(log["stream"].strip())

        logger.info(f"Successfully built image: {image.id}")

        if no_tag_latest is False:
            logger.info("Tagging image latest")
            tag_image(image=image, repository=repository, new_tag="latest")

        return image

    except (BuildError, DockerException) as e:
        logger.error(f"{str(e)}")
        raise e
    finally:
        client.close()


def build_docker(args):
    # Enforce our commit tag
    if args.no_tagged_commit is False:
        is_on_tagged_commit()

    # Get our registry and image_name
    if args.private_repo is not None:
        registry = args.private_repo
        image_name = f"{args.private_repo}/{args.username}/{args.repo_name}"
    else:
        registry = "https://index.docker.io/v1/"
        image_name = f"{args.username}/{args.repo_name}"

    # Build our container
    build_image(tag=args.tag, repository=registry, no_tag_latest=args.no_tag_latest)

    # Upload our image
    if args.no_upload is False:
        upload_image(
            username=args.username,
            image_name=image_name,
            secret=args.secret,
            registry=registry,
        )
