import unittest
from unittest.mock import patch
import logging
from builder.arch_package.arch_package import get_built_files

# Setup logging for tests
logging.basicConfig(level=logging.INFO)


class TestGetBuiltFiles(unittest.TestCase):

    @patch("builder.arch_package.arch_package.os.listdir")
    @patch("builder.arch_package.arch_package.logger")
    def test_get_built_files_success_unsigned(self, mock_logger, mock_listdir):
        # Setup mock
        before_files = ["existing1", "existing2"]
        mock_listdir.return_value = ["pkg-file.pkg", "existing1", "existing2"]

        # Call the function
        result = get_built_files(before_files, signed=False, package_name="pkg-file")

        # Assertions
        self.assertEqual(result.package, "pkg-file.pkg")
        self.assertIsNone(result.signature)
        mock_logger.error.assert_not_called()

    @patch("builder.arch_package.arch_package.os.listdir")
    @patch("builder.arch_package.arch_package.logger")
    def test_get_built_files_success_signed(self, mock_logger, mock_listdir):
        # Setup mock
        before_files = ["existing1", "existing2"]
        mock_listdir.return_value = [
            "pkg-file.pkg",
            "pkg-file.sig",
            "existing1",
            "existing2",
        ]

        # Call the function
        result = get_built_files(before_files, signed=True, package_name="pkg-file")

        # Assertions
        self.assertEqual(result.package, "pkg-file.pkg")
        self.assertEqual(result.signature, "pkg-file.sig")
        mock_logger.error.assert_not_called()

    @patch("builder.arch_package.arch_package.os.listdir")
    @patch("builder.arch_package.arch_package.logger")
    def test_get_built_files_no_package(self, mock_logger, mock_listdir):
        # Setup mock
        before_files = ["existing1", "existing2"]
        mock_listdir.return_value = ["pkg-file.sig", "existing1", "existing2"]

        # Call the function and expect a ValueError
        with self.assertRaises(ValueError) as cm:
            get_built_files(before_files, signed=False, package_name="pkg-file")

        self.assertEqual(str(cm.exception), "Failed to detect built package")
        mock_logger.error.assert_not_called()

    @patch("builder.arch_package.arch_package.os.listdir")
    @patch("builder.arch_package.arch_package.logger")
    def test_get_built_files_no_signature(self, mock_logger, mock_listdir):
        # Setup mock for os.listdir
        before_files = ["existing1", "existing2"]
        mock_listdir.return_value = [
            "pkg-file.pkg",
            "pkg2-file.sig",
            "existing1",
            "existing2",
        ]

        # Call the function and expect a ValueError
        with self.assertRaises(ValueError) as cm:
            get_built_files(before_files, signed=True, package_name="pkg-file")

        self.assertEqual(str(cm.exception), "Failed to detect build package signature")
        mock_logger.error.assert_not_called()

    @patch("builder.arch_package.arch_package.os.listdir")
    @patch("builder.arch_package.arch_package.logger")
    def test_too_many_files_with_signature(self, mock_logger, mock_listdir):
        # Setup mock for os.listdir
        before_files = ["existing1", "existing2"]
        mock_listdir.return_value = [
            "pkg-file.pkg",
            "pkg-file-v1.1.0.pkg",
            "pkg-file.sig",
            "existing1",
            "existing2",
        ]

        # Call the function and expect a ValueError
        with self.assertRaises(ValueError) as cm:
            get_built_files(before_files, signed=True, package_name="pkg-file")

        self.assertEqual(str(cm.exception), "We have too many files... 3")

        # Check that the error was logged with the expected files
        mock_logger.error.assert_called_once()
        logged_message = mock_logger.error.call_args[0][0]
        expected_files = ["pkg-file.pkg", "pkg-file.sig", "pkg-file-v1.1.0.pkg"]
        self.assertCountEqual(logged_message, expected_files)

    @patch("builder.arch_package.arch_package.os.listdir")
    @patch("builder.arch_package.arch_package.logger")
    def test_too_many_files_no_signature(self, mock_logger, mock_listdir):
        # Setup mock for os.listdir
        before_files = ["existing1", "existing2"]
        mock_listdir.return_value = [
            "pkg-file.pkg",
            "pkg-file-v1.1.0.pkg",
            "existing1",
            "existing2",
        ]

        # Call the function and expect a ValueError
        with self.assertRaises(ValueError) as cm:
            get_built_files(before_files, signed=False, package_name="pkg-file")

        self.assertEqual(str(cm.exception), "We have too many files... 2")

        # Check that the error was logged with the expected files
        mock_logger.error.assert_called_once()
        logged_message = mock_logger.error.call_args[0][0]
        expected_files = ["pkg-file.pkg", "pkg-file-v1.1.0.pkg"]
        self.assertCountEqual(logged_message, expected_files)

    @patch("builder.arch_package.arch_package.os.listdir")
    @patch("builder.arch_package.arch_package.logger")
    def test_cant_detect_signature(self, mock_logger, mock_listdir):
        # Setup mock for os.listdir
        before_files = ["existing1", "existing2"]
        mock_listdir.return_value = [
            "pkg-file.pkg",
            "pkg-file-v1.1.0.pkg",
            "existing1",
            "existing2",
        ]

        # Call the function and expect a ValueError
        with self.assertRaises(ValueError) as cm:
            get_built_files(before_files, signed=True, package_name="pkg-file")

        self.assertEqual(str(cm.exception), "Failed to detect build package signature")

        mock_logger.error.assert_not_called()

    @patch("builder.arch_package.arch_package.os.listdir")
    @patch("builder.arch_package.arch_package.logger")
    def test_cant_detect_no_signature(self, mock_logger, mock_listdir):
        # Setup mock for os.listdir
        before_files = ["existing1", "existing2"]
        mock_listdir.return_value = [
            "pkg2-file.pkg",
            "pkg2-file-v1.1.0.pkg",
            "existing1",
            "existing2",
        ]

        # Call the function and expect a ValueError
        with self.assertRaises(ValueError) as cm:
            get_built_files(before_files, signed=False, package_name="pkg-file")

        self.assertEqual(str(cm.exception), "Failed to detect built package")

        mock_logger.error.assert_not_called()


if __name__ == "__main__":
    unittest.main()
