import unittest
from unittest.mock import patch, mock_open
from builder.arch_package.arch_package import create_gpg_key


class TestCreateGpgKey(unittest.TestCase):

    @patch("builder.arch_package.arch_package.os.remove")
    @patch("builder.arch_package.arch_package.os.path.exists", return_value=True)
    @patch("builder.arch_package.arch_package.run_shell_command")
    @patch("builtins.open", new_callable=mock_open)
    @patch(
        "builder.arch_package.arch_package.base64.b64decode",
        return_value=b"decoded_secret",
    )
    @patch("builder.arch_package.arch_package.logger")
    def test_create_gpg_key_success(
        self,
        mock_logger,
        mock_b64decode,
        mock_open,
        mock_run_shell_command,
        mock_exists,
        mock_remove,
    ):
        signing_secret = "test_secret"

        # Call the function
        create_gpg_key(signing_secret)

        # Verify the logger was called correctly
        mock_logger.info.assert_called_once_with("Setting up the signing secret...")

        # Verify the base64 decoding
        mock_b64decode.assert_called_once_with(signing_secret)

        # Verify the file operations
        mock_open.assert_called_once_with("/tmp/private.pgp", "wb")
        mock_open().write.assert_called_once_with(b"decoded_secret")

        # Verify the GPG import command
        mock_run_shell_command.assert_called_once_with(
            ["gpg", "--import", "/tmp/private.pgp"]
        )

        # Verify the cleanup
        mock_exists.assert_called_once_with("/tmp/private.pgp")
        mock_remove.assert_called_once_with("/tmp/private.pgp")

    @patch("builder.arch_package.arch_package.os.remove")
    @patch("builder.arch_package.arch_package.os.path.exists", return_value=False)
    @patch("builder.arch_package.arch_package.run_shell_command")
    @patch("builtins.open", new_callable=mock_open)
    @patch(
        "builder.arch_package.arch_package.base64.b64decode",
        return_value=b"decoded_secret",
    )
    @patch("builder.arch_package.arch_package.logger")
    def test_create_gpg_key_no_cleanup(
        self,
        mock_logger,
        mock_b64decode,
        mock_open,
        mock_run_shell_command,
        mock_exists,
        mock_remove,
    ):
        signing_secret = "test_secret"

        # Call the function
        create_gpg_key(signing_secret)

        # Verify the logger was called correctly
        mock_logger.info.assert_called_once_with("Setting up the signing secret...")

        # Verify the base64 decoding
        mock_b64decode.assert_called_once_with(signing_secret)

        # Verify the file operations
        mock_open.assert_called_once_with("/tmp/private.pgp", "wb")
        mock_open().write.assert_called_once_with(b"decoded_secret")

        # Verify the GPG import command
        mock_run_shell_command.assert_called_once_with(
            ["gpg", "--import", "/tmp/private.pgp"]
        )

        # Verify the cleanup
        mock_exists.assert_called_once_with("/tmp/private.pgp")
        mock_remove.assert_not_called()

    @patch("builder.arch_package.arch_package.os.remove")
    @patch("builder.arch_package.arch_package.os.path.exists", return_value=True)
    @patch("builder.arch_package.arch_package.run_shell_command")
    @patch("builtins.open", new_callable=mock_open)
    @patch(
        "builder.arch_package.arch_package.base64.b64decode",
        return_value=b"decoded_secret",
    )
    @patch("builder.arch_package.arch_package.logger")
    def test_create_gpg_key_exception(
        self,
        mock_logger,
        mock_b64decode,
        mock_open,
        mock_run_shell_command,
        mock_exists,
        mock_remove,
    ):
        signing_secret = "test_secret"
        mock_run_shell_command.side_effect = Exception("GPG import failed")

        with self.assertRaises(Exception):
            create_gpg_key(signing_secret)

        # Verify the logger was called correctly
        mock_logger.info.assert_called_once_with("Setting up the signing secret...")

        # Verify the base64 decoding
        mock_b64decode.assert_called_once_with(signing_secret)

        # Verify the file operations
        mock_open.assert_called_once_with("/tmp/private.pgp", "wb")
        mock_open().write.assert_called_once_with(b"decoded_secret")

        # Verify the GPG import command
        mock_run_shell_command.assert_called_once_with(
            ["gpg", "--import", "/tmp/private.pgp"]
        )

        # Verify the cleanup
        mock_exists.assert_called_once_with("/tmp/private.pgp")
        mock_remove.assert_called_once_with("/tmp/private.pgp")


if __name__ == "__main__":
    unittest.main()
