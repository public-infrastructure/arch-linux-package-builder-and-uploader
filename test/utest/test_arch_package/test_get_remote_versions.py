import unittest
from builder.arch_package.arch_package import get_remote_versions


class TestVerifyRemotePackage(unittest.TestCase):

    def test_get_remote_versions(self):
        packages = [
            "builder-v0.3.0-1-any.pkg.tar.zst",
            "notifier-v10.0.0-1-any.pkg.tar.zst",
            "tester-v0.2.0-1-x86_64.pkg.tar.zst" "invalid-vX.X.X-1-any.pkg.tar.zst",
            "no-version-info.pkg.tar.zst",
        ]
        expected_versions = ["v0.3.0", "v10.0.0", "v0.2.0"]
        versions = get_remote_versions(packages)
        self.assertEqual(versions, expected_versions)


if __name__ == "__main__":
    unittest.main()
