import unittest
from builder.arch_package.arch_package import extract_version


class TestExtractVersion(unittest.TestCase):

    def test_extract_version(self):
        filenames = [
            "builder-v0.3.0-1-any.pkg.tar.zst",
            "istio-v1.20.1-1-x86_64.pkg.tar.zst",
            "notifier-v10.0.0-1-any.pkg.tar.zst",
            "tester-v0.2.0-1-x86_64.pkg.tar.zst",
            "invalid-vX.X.X-1-any.pkg.tar.zst",
            "no-version-info.pkg.tar.zst",
        ]
        expected_versions = ["v0.3.0", "v1.20.1", "v10.0.0", "v0.2.0", None, None]
        for filename, expected_version in zip(filenames, expected_versions):
            self.assertEqual(extract_version(filename), expected_version)


if __name__ == "__main__":
    unittest.main()
