import unittest
from builder.arch_package.arch_package import get_sorted_versions


class TestVerifyRemotePackage(unittest.TestCase):

    def test_get_sorted_versions(self):
        packages = [
            "builder-v0.3.0-1-any.pkg.tar.zst",
            "notifier-v10.0.0-1-any.pkg.tar.zst",
            "tester-v0.2.0-1-x86_64.pkg.tar.zst" "invalid-vX.X.X-1-any.pkg.tar.zst",
            "no-version-info.pkg.tar.zst",
        ]
        expected_sorted_versions = ["v10.0.0", "v0.3.0", "v0.2.0"]
        sorted_versions = get_sorted_versions(packages)
        self.assertEqual(sorted_versions, expected_sorted_versions)


if __name__ == "__main__":
    unittest.main()
