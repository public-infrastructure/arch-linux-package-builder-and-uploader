import unittest
from unittest.mock import patch, Mock
from builder.arch_package.arch_package import get_remote_packages


class TestGetRemotePackages(unittest.TestCase):

    @patch("builder.arch_package.arch_package.requests.get")
    def test_get_remote_packages(self, mock_get):
        html_content = """
        <html>
        <body>
        <a href="builder-v0.3.0-1-any.pkg.tar.zst">builder-v0.3.0-1-any.pkg.tar.zst</a>
        <a href="istio-1.20.1-1-x86_64.pkg.tar.zst">istio-1.20.1-1-x86_64.pkg.tar.zst</a>
        <a href="notifier-v10.0.0-1-any.pkg.tar.zst">notifier-v10.0.0-1-any.pkg.tar.zst</a>
        <a href="tester-v0.2.0-1-x86_64.pkg.tar.zst">tester-v0.2.0-1-x86_64.pkg.tar.zst</a>
        </body>
        </html>
        """
        mock_get.return_value = Mock(ok=True)
        mock_get.return_value.content = html_content

        base_mirror_url = "http://example.com"
        package_name = "builder"
        expected_files = ["builder-v0.3.0-1-any.pkg.tar.zst"]
        files = get_remote_packages(base_mirror_url, package_name)

        mock_get.assert_called_once_with(base_mirror_url)
        self.assertEqual(files, expected_files)


if __name__ == "__main__":
    unittest.main()
