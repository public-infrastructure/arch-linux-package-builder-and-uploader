import unittest
from unittest.mock import patch, mock_open, MagicMock
from builder.arch_package.arch_package import upload_package, PackageFiles
import requests  # type: ignore[import-untyped]


class TestUploadPackage(unittest.TestCase):

    @patch("builder.arch_package.arch_package.requests.post")
    @patch("builtins.open", new_callable=mock_open, read_data=b"file content")
    @patch("builder.arch_package.arch_package.logger")
    def test_upload_package_without_signature_success(
        self, mock_logger, mock_open, mock_post
    ):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_post.return_value = mock_response

        built_files = PackageFiles(package="path/to/package")
        upload_package(
            built_files,
            signed=False,
            package_name="test_package",
            upload_secret="secret",
            upload_url="http://localhost",
        )

        mock_open.assert_called_once_with("path/to/package", "rb")
        mock_post.assert_called_once()
        mock_logger.info.assert_called_once_with("Uploading our file...")
        mock_logger.error.assert_not_called()

    @patch("builder.arch_package.arch_package.requests.post")
    @patch("builtins.open", new_callable=mock_open, read_data=b"file content")
    @patch("builder.arch_package.arch_package.logger")
    def test_upload_package_with_signature_success(
        self, mock_logger, mock_open, mock_post
    ):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_post.return_value = mock_response

        built_files = PackageFiles(
            package="path/to/package", signature="path/to/signature"
        )
        upload_package(
            built_files,
            signed=True,
            package_name="test_package",
            upload_secret="secret",
            upload_url="http://localhost",
        )

        self.assertEqual(mock_open.call_count, 2)
        mock_post.assert_called_once()
        mock_logger.info.assert_called_once_with("Uploading our file...")
        mock_logger.error.assert_not_called()

    @patch("builder.arch_package.arch_package.requests.post")
    @patch("builtins.open", new_callable=mock_open, read_data=b"file content")
    @patch("builder.arch_package.arch_package.logger")
    def test_upload_package_fail(self, mock_logger, mock_open, mock_post):
        mock_response = MagicMock()
        mock_response.status_code = 500
        mock_response.raise_for_status.side_effect = requests.exceptions.HTTPError
        mock_post.return_value = mock_response

        built_files = PackageFiles(package="path/to/package")
        with self.assertRaises(requests.exceptions.HTTPError):
            upload_package(
                built_files,
                signed=False,
                package_name="test_package",
                upload_secret="secret",
                upload_url="http://localhost",
            )


if __name__ == "__main__":
    unittest.main()
