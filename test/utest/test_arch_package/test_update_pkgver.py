import unittest
from unittest.mock import patch, mock_open
from builder.arch_package.arch_package import update_pkgver, UpdateError


class TestUpdatePkgver(unittest.TestCase):

    @patch(
        "builder.arch_package.arch_package.open",
        new_callable=mock_open,
        read_data="pkgver=1.0.0\n",
    )
    @patch("builder.arch_package.arch_package.logger")
    def test_update_pkgver_success(self, mock_logger, mock_open):
        file_path = "test_file"
        tag = "2.0.0"

        # Call the function
        update_pkgver(file_path, tag)

        # Verify the logger was called correctly
        mock_logger.info.assert_called_once_with(
            f"Updating our package version to {tag} in the file {file_path}"
        )

        # Verify file operations
        mock_open.assert_called_with(file_path, "w")
        mock_open().read.assert_called_once()
        mock_open().write.assert_called_once_with(f"pkgver={tag}\n")

    @patch(
        "builder.arch_package.arch_package.open",
        new_callable=mock_open,
        read_data="some other line\n",
    )
    @patch("builder.arch_package.arch_package.logger")
    def test_update_pkgver_missing_pkgver(self, mock_logger, mock_open):
        file_path = "test_file"
        tag = "2.0.0"

        # Call the function and expect an UpdateError
        with self.assertRaises(UpdateError) as cm:
            update_pkgver(file_path, tag)

        self.assertEqual(
            str(cm.exception),
            "The line containing 'pkgver=' was not found in the file.",
        )

        # Verify the logger was called correctly
        mock_logger.info.assert_called_once_with(
            f"Updating our package version to {tag} in the file {file_path}"
        )

    @patch(
        "builder.arch_package.arch_package.open",
        new_callable=mock_open,
        read_data="pkgver=1.0.0\n",
    )
    @patch("builder.arch_package.arch_package.logger")
    @patch(
        "builder.arch_package.arch_package.re.subn", return_value=("pkgver=2.0.0\n", 0)
    )
    def test_update_pkgver_no_replacements(self, mock_re_subn, mock_logger, mock_open):
        file_path = "test_file"
        tag = "2.0.0"

        # Call the function and expect an UpdateError
        with self.assertRaises(UpdateError) as cm:
            update_pkgver(file_path, tag)

        self.assertEqual(str(cm.exception), "Failed to update 'pkgver=' line.")

        # Verify the logger was called correctly
        mock_logger.info.assert_called_once_with(
            f"Updating our package version to {tag} in the file {file_path}"
        )


if __name__ == "__main__":
    unittest.main()
