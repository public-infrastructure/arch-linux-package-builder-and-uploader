import unittest
from unittest.mock import patch, Mock

from builder.arch_package.arch_package import build_arch_package, PackageFiles


class TestBuildArchPackage(unittest.TestCase):

    @patch("builder.arch_package.arch_package.verify_remote_package")
    @patch("builder.arch_package.arch_package.create_gpg_key")
    @patch("builder.arch_package.arch_package.update_pkgver")
    @patch("builder.arch_package.arch_package.get_built_files")
    @patch("builder.arch_package.arch_package.upload_package")
    @patch("builder.arch_package.arch_package.run_shell_command")
    @patch("builder.arch_package.arch_package.os.listdir")
    @patch("builder.arch_package.arch_package.is_on_tagged_commit")
    def test_build_arch_package_full_flow(
        self,
        mock_is_on_tagged_commit,
        mock_listdir,
        mock_run_shell_command,
        mock_upload_package,
        mock_get_built_files,
        mock_update_pkgver,
        mock_create_gpg_key,
        mock_verify_remote_package,
    ):
        # Setup mocks
        mock_is_on_tagged_commit.return_value = "v1.0.0"
        mock_listdir.return_value = ["existing_file"]
        mock_get_built_files.return_value = PackageFiles(
            package="pkg-file.pkg", signature="pkg-file.sig"
        )

        # Define arguments
        args = Mock()
        args.no_tagged_commit = False
        args.no_upload = False
        args.sign = True
        args.no_update_pkgbuild_version = False
        args.base_mirror_url = "http://example.com/mirror"
        args.package_name = "pkg-file"
        args.secret = "secret-token"
        args.upload_url = "http://example.com/upload"
        args.branch = "feature-branch"
        args.gpg_key = "gpg-key"

        # Call the function
        build_arch_package(args)

        # Verify calls
        mock_is_on_tagged_commit.assert_called_once()
        mock_verify_remote_package.assert_called_once_with(
            base_mirror_url="http://example.com/mirror",
            package_name="pkg-file",
            tag="v1.0.0",
        )
        mock_create_gpg_key.assert_called_once_with("gpg-key")
        mock_update_pkgver.assert_called_once_with("PKGBUILD", "v1.0.0")
        mock_run_shell_command.assert_called_once_with(["/usr/bin/makepkg", "--sign"])
        mock_get_built_files.assert_called_once_with(
            before_files=["existing_file"], signed=True, package_name="pkg-file"
        )
        mock_upload_package.assert_called_once_with(
            built_files=PackageFiles(package="pkg-file.pkg", signature="pkg-file.sig"),
            signed=True,
            package_name="pkg-file",
            upload_secret="secret-token",
            upload_url="http://example.com/upload",
        )

    @patch("builder.arch_package.arch_package.verify_remote_package")
    @patch("builder.arch_package.arch_package.create_gpg_key")
    @patch("builder.arch_package.arch_package.update_pkgver")
    @patch("builder.arch_package.arch_package.get_built_files")
    @patch("builder.arch_package.arch_package.upload_package")
    @patch("builder.arch_package.arch_package.run_shell_command")
    @patch("builder.arch_package.arch_package.os.listdir")
    @patch("builder.arch_package.arch_package.is_on_tagged_commit")
    def test_build_arch_package_no_upload(
        self,
        mock_is_on_tagged_commit,
        mock_listdir,
        mock_run_shell_command,
        mock_upload_package,
        mock_get_built_files,
        mock_update_pkgver,
        mock_create_gpg_key,
        mock_verify_remote_package,
    ):
        # Setup mocks
        mock_is_on_tagged_commit.return_value = "v1.0.0"
        mock_listdir.return_value = ["existing_file"]
        mock_get_built_files.return_value = PackageFiles(package="pkg-file.pkg")

        # Define arguments
        args = Mock()
        args.no_tagged_commit = False
        args.no_upload = True
        args.sign = True
        args.no_update_pkgbuild_version = False
        args.base_mirror_url = "http://example.com/mirror"
        args.package_name = "pkg-file"
        args.secret = "secret-token"
        args.upload_url = "http://example.com/upload"
        args.branch = "feature-branch"
        args.gpg_key = "gpg-key"

        # Call the function
        build_arch_package(args)

        # Verify calls
        mock_is_on_tagged_commit.assert_called_once()
        mock_update_pkgver.assert_called_once_with("PKGBUILD", "v1.0.0")
        mock_run_shell_command.assert_called_once_with(["/usr/bin/makepkg", "--sign"])
        mock_get_built_files.assert_not_called()
        mock_upload_package.assert_not_called()
        mock_create_gpg_key.assert_called_once_with("gpg-key")
        mock_verify_remote_package.assert_not_called()

    @patch("builder.arch_package.arch_package.verify_remote_package")
    @patch("builder.arch_package.arch_package.create_gpg_key")
    @patch("builder.arch_package.arch_package.update_pkgver")
    @patch("builder.arch_package.arch_package.get_built_files")
    @patch("builder.arch_package.arch_package.upload_package")
    @patch("builder.arch_package.arch_package.run_shell_command")
    @patch("builder.arch_package.arch_package.os.listdir")
    @patch("builder.arch_package.arch_package.is_on_tagged_commit")
    def test_build_arch_package_no_sign(
        self,
        mock_is_on_tagged_commit,
        mock_listdir,
        mock_run_shell_command,
        mock_upload_package,
        mock_get_built_files,
        mock_update_pkgver,
        mock_create_gpg_key,
        mock_verify_remote_package,
    ):
        # Setup mocks
        mock_is_on_tagged_commit.return_value = "v1.0.0"
        mock_listdir.return_value = ["existing_file"]
        mock_get_built_files.return_value = PackageFiles(package="pkg-file.pkg")

        # Define arguments
        args = Mock()
        args.no_tagged_commit = False
        args.no_upload = False
        args.sign = False
        args.no_update_pkgbuild_version = False
        args.base_mirror_url = "http://example.com/mirror"
        args.package_name = "pkg-file"
        args.secret = "secret-token"
        args.upload_url = "http://example.com/upload"
        args.branch = "feature-branch"
        args.gpg_key = "gpg-key"

        # Call the function
        build_arch_package(args)

        # Verify calls
        mock_is_on_tagged_commit.assert_called_once()
        mock_verify_remote_package.assert_called_once_with(
            base_mirror_url="http://example.com/mirror",
            package_name="pkg-file",
            tag="v1.0.0",
        )
        mock_create_gpg_key.assert_not_called()
        mock_update_pkgver.assert_called_once_with("PKGBUILD", "v1.0.0")
        mock_run_shell_command.assert_called_once_with(["/usr/bin/makepkg"])
        mock_get_built_files.assert_called_once_with(
            before_files=["existing_file"], signed=False, package_name="pkg-file"
        )
        mock_upload_package.assert_called_once_with(
            built_files=PackageFiles(package="pkg-file.pkg"),
            signed=False,
            package_name="pkg-file",
            upload_secret="secret-token",
            upload_url="http://example.com/upload",
        )

    @patch("builder.arch_package.arch_package.verify_remote_package")
    @patch("builder.arch_package.arch_package.create_gpg_key")
    @patch("builder.arch_package.arch_package.update_pkgver")
    @patch("builder.arch_package.arch_package.get_built_files")
    @patch("builder.arch_package.arch_package.upload_package")
    @patch("builder.arch_package.arch_package.run_shell_command")
    @patch("builder.arch_package.arch_package.os.listdir")
    @patch("builder.arch_package.arch_package.is_on_tagged_commit")
    def test_build_arch_package_no_update_pkgbuild_version(
        self,
        mock_is_on_tagged_commit,
        mock_listdir,
        mock_run_shell_command,
        mock_upload_package,
        mock_get_built_files,
        mock_update_pkgver,
        mock_create_gpg_key,
        mock_verify_remote_package,
    ):
        # Setup mocks
        mock_is_on_tagged_commit.return_value = "v1.0.0"
        mock_listdir.return_value = ["existing_file"]
        mock_get_built_files.return_value = PackageFiles(package="pkg-file.pkg")

        # Define arguments
        args = Mock()
        args.no_tagged_commit = False
        args.no_upload = False
        args.sign = True
        args.no_update_pkgbuild_version = True
        args.base_mirror_url = "http://example.com/mirror"
        args.package_name = "pkg-file"
        args.secret = "secret-token"
        args.upload_url = "http://example.com/upload"
        args.branch = "feature-branch"
        args.gpg_key = "gpg-key"

        # Call the function
        build_arch_package(args)

        # Verify calls
        mock_is_on_tagged_commit.assert_called_once()
        mock_verify_remote_package.assert_called_once_with(
            base_mirror_url="http://example.com/mirror",
            package_name="pkg-file",
            tag="v1.0.0",
        )
        mock_create_gpg_key.assert_called_once_with("gpg-key")
        mock_update_pkgver.assert_not_called()
        mock_run_shell_command.assert_called_once_with(["/usr/bin/makepkg", "--sign"])
        mock_get_built_files.assert_called_once_with(
            before_files=["existing_file"], signed=True, package_name="pkg-file"
        )
        mock_upload_package.assert_called_once_with(
            built_files=PackageFiles(package="pkg-file.pkg"),
            signed=True,
            package_name="pkg-file",
            upload_secret="secret-token",
            upload_url="http://example.com/upload",
        )


if __name__ == "__main__":
    unittest.main()
