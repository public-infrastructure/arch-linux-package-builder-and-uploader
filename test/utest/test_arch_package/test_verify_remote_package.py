import unittest
from unittest.mock import patch
from builder.arch_package.arch_package import verify_remote_package


class TestVerifyRemotePackage(unittest.TestCase):

    @patch("builder.arch_package.arch_package.get_remote_packages")
    @patch("builder.arch_package.arch_package.get_remote_versions")
    @patch("builder.arch_package.arch_package.get_sorted_versions")
    @patch("builder.arch_package.arch_package.logger")
    def test_verify_remote_package(
        self,
        mock_log,
        mock_get_sorted_versions,
        mock_get_remote_versions,
        mock_get_remote_packages,
    ):
        base_mirror_url = "http://example.com"
        package_name = "builder"
        tag = "v0.3.1"

        # Case: No remote packages
        mock_get_remote_packages.return_value = []
        verify_remote_package(base_mirror_url, package_name, tag)
        mock_get_remote_packages.assert_called_once_with(
            base_mirror_url=base_mirror_url, package_name=package_name
        )
        mock_get_remote_versions.assert_not_called()
        mock_get_sorted_versions.assert_not_called()
        mock_log.info.assert_not_called()

        # Case: Remote packages exist, tag not latest
        mock_get_remote_packages.reset_mock()
        mock_get_remote_versions.reset_mock()
        mock_get_sorted_versions.reset_mock()
        mock_log.reset_mock()

        mock_get_remote_packages.return_value = ["builder-v0.3.0-1-any.pkg.tar.zst"]
        mock_get_remote_versions.return_value = ["v0.3.0"]
        mock_get_sorted_versions.return_value = ["v0.4.1", "v0.3.0"]
        with self.assertRaises(Exception) as context:
            verify_remote_package(base_mirror_url, package_name, tag)
        self.assertIn("Cannot build due to version issue", str(context.exception))
        mock_log.info.assert_called_once()

        # Case: Remote packages exist, tag is latest but already uploaded
        mock_get_remote_packages.reset_mock()
        mock_get_remote_versions.reset_mock()
        mock_get_sorted_versions.reset_mock()
        mock_log.reset_mock()

        mock_get_remote_packages.return_value = ["builder-v0.3.1-1-any.pkg.tar.zst"]
        mock_get_remote_versions.return_value = ["v0.3.1"]
        mock_get_sorted_versions.return_value = ["v0.3.1"]
        with self.assertRaises(Exception) as context:
            verify_remote_package(base_mirror_url, package_name, tag)
        self.assertIn("Cannot build due to version issue", str(context.exception))
        mock_log.info.assert_called_once()


if __name__ == "__main__":
    unittest.main()


if __name__ == "__main__":
    unittest.main()
