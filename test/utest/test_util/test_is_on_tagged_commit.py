import unittest
from unittest.mock import patch, MagicMock, call
from builder.util.util import is_on_tagged_commit, NoTaggedCommitFound


class TestIsOnTaggedCommit(unittest.TestCase):

    @patch("builder.util.util.run_shell_command")
    @patch("builder.util.util.logger")
    def test_is_on_tagged_commit_success(self, mock_logger, mock_run_shell_command):
        # Mocking the outputs of the git commands
        mock_run_shell_command.side_effect = [
            MagicMock(stdout="commit_id_1\n"),  # git rev-parse HEAD
            MagicMock(stdout="tag1\ntag2\n"),  # git tag
            MagicMock(stdout="commit_id_2\n"),  # git rev-list -n 1 tag1
            MagicMock(stdout="commit_id_1\n"),  # git rev-list -n 1 tag2
        ]

        # Call the function
        tag = is_on_tagged_commit()

        # Check if the return value is as expected
        self.assertEqual(tag, "tag2")

        # Verify that the logger was called with the correct messages
        expected_calls = [
            call.info("Enforcing that we are on a tagged branch..."),
            call.info("Tagged commit: tag2"),
        ]
        mock_logger.info.assert_has_calls(expected_calls)
        self.assertEqual(mock_logger.info.call_count, 2)

    @patch("builder.util.util.run_shell_command")
    @patch("builder.util.util.logger")
    def test_is_on_tagged_commit_no_tag(self, mock_logger, mock_run_shell_command):
        # Mocking the outputs of the git commands
        mock_run_shell_command.side_effect = [
            MagicMock(stdout="commit_id_2\n"),  # git rev-parse HEAD
            MagicMock(stdout="tag1\ntag2\n"),  # git tag
            MagicMock(stdout="commit_id_1\n"),  # git rev-list -n 1 tag1
            MagicMock(stdout="commit_id_3\n"),  # git rev-list -n 1 tag2
        ]

        # Check if the function raises the NoTaggedCommitFound exception
        with self.assertRaises(NoTaggedCommitFound):
            is_on_tagged_commit()

        # Verify that the logger was called with the correct warning message
        expected_calls = [
            call.info("Enforcing that we are on a tagged branch..."),
            call.info(
                "\nThere are no tags associated with the current commit hash.\nWe will not be uploading this build so we will not build it...\nTags and their commit IDs:\n\nCurrent commit:   commit_id_2\n"
            ),
            call.info(
                "No tag found. If you're not building on a tagged commit you MUST specify --no-tagged-commit --branch {BRANCH_NAME}"
            ),
        ]
        mock_logger.info.assert_has_calls(expected_calls)
        self.assertEqual(mock_logger.info.call_count, 3)

    @patch("builder.util.util.run_shell_command")
    @patch("builder.util.util.logger")
    def test_is_on_tagged_commit_no_tags_at_all(
        self, mock_logger, mock_run_shell_command
    ):
        # Mocking the outputs of the git commands
        mock_run_shell_command.side_effect = [
            MagicMock(stdout="commit_id_2\n"),  # git rev-parse HEAD
            MagicMock(stdout=""),  # git tag returns empty
        ]

        # Check if the function raises the NoTaggedCommitFound exception
        with self.assertRaises(NoTaggedCommitFound):
            is_on_tagged_commit()

        # Verify that the logger was called with the correct warning message
        expected_calls = [
            call.info("Enforcing that we are on a tagged branch..."),
            call.info(
                "\nThere are no tags associated with the current commit hash.\nWe will not be uploading this build so we will not build it...\nTags and their commit IDs:\n\nCurrent commit:   commit_id_2\n"
            ),
            call.info(
                "No tag found. If you're not building on a tagged commit you MUST specify --no-tagged-commit --branch {BRANCH_NAME}"
            ),
        ]
        mock_logger.info.assert_has_calls(expected_calls)
        self.assertEqual(mock_logger.info.call_count, 3)


if __name__ == "__main__":
    unittest.main()
