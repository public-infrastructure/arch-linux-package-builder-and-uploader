import unittest
from unittest.mock import patch, MagicMock
from docker.errors import APIError  # type: ignore[import-untyped]
from builder.docker.docker import tag_image


class TestTagImage(unittest.TestCase):

    @patch("builder.docker.docker.docker.from_env")
    @patch("builder.docker.docker.logger")
    def test_tag_image_success(self, mock_logger, mock_from_env):
        # Setup mocks
        mock_client = MagicMock()
        mock_from_env.return_value = mock_client
        mock_image = MagicMock()
        mock_client.images.get.return_value = mock_image
        mock_image.tag.return_value = None  # Simulate successful tagging

        # Call the function
        tag_image(mock_image, "repository", "new_tag")

        # Assertions
        mock_image.tag.assert_called_once_with(repository="repository", tag="new_tag")
        mock_logger.info.assert_called_once_with(
            f"Image {mock_image.id} tagged as repository:new_tag"
        )

    @patch("builder.docker.docker.docker.from_env")
    @patch("builder.docker.docker.logger")
    def test_tag_image_api_error(self, mock_logger, mock_from_env):
        # Setup mocks
        mock_client = MagicMock()
        mock_from_env.return_value = mock_client
        mock_image = MagicMock()
        mock_client.images.get.return_value = mock_image
        mock_image.tag.side_effect = APIError("API error")

        # Call the function
        tag_image(mock_image, "repository", "new_tag")

        # Assertions
        mock_logger.error.assert_called_once_with("Error occurred: API error")

    @patch("builder.docker.docker.docker.from_env")
    @patch("builder.docker.docker.logger")
    def test_tag_image_unexpected_error(self, mock_logger, mock_from_env):
        # Setup mocks
        mock_client = MagicMock()
        mock_from_env.return_value = mock_client
        mock_image = MagicMock()
        mock_client.images.get.return_value = mock_image
        mock_image.tag.side_effect = Exception("Unexpected error")

        # Call the function
        tag_image(mock_image, "repository", "new_tag")

        # Assertions
        mock_logger.error.assert_called_once_with("Unexpected error: Unexpected error")


if __name__ == "__main__":
    unittest.main()
