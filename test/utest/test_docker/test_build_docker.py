import unittest
from unittest.mock import patch
from argparse import Namespace
from builder.docker.docker import build_docker


class TestBuildDocker(unittest.TestCase):

    @patch("builder.docker.docker.upload_image")
    @patch("builder.docker.docker.build_image")
    @patch("builder.docker.docker.is_on_tagged_commit")
    def test_build_docker_no_tagged_commit(
        self, mock_is_on_tagged_commit, mock_build_image, mock_upload_image
    ):
        args = Namespace(
            command="docker",
            no_tagged_commit=False,
            tag="foo",
            no_tag_latest=False,
            secret="secret",
            no_upload=True,
            username="username",
            repo_name="repo_name",
            private_repo=None,
        )

        build_docker(args)

        mock_is_on_tagged_commit.assert_called_once()
        mock_build_image.assert_called_once_with(
            tag="foo", repository="https://index.docker.io/v1/", no_tag_latest=False
        )
        mock_upload_image.assert_not_called()

    @patch("builder.docker.docker.upload_image")
    @patch("builder.docker.docker.build_image")
    @patch("builder.docker.docker.is_on_tagged_commit")
    def test_build_docker_tagged_commit(
        self, mock_is_on_tagged_commit, mock_build_image, mock_upload_image
    ):
        args = Namespace(
            command="docker",
            no_tagged_commit=True,
            tag="foo",
            no_tag_latest=False,
            secret="secret",
            no_upload=False,
            username="username",
            repo_name="repo_name",
            private_repo=None,
        )

        build_docker(args)

        mock_is_on_tagged_commit.assert_not_called()
        mock_build_image.assert_called_once_with(
            tag="foo", repository="https://index.docker.io/v1/", no_tag_latest=False
        )
        mock_upload_image.assert_called_once_with(
            username="username",
            image_name="username/repo_name",
            secret="secret",
            registry="https://index.docker.io/v1/",
        )

    @patch("builder.docker.docker.upload_image")
    @patch("builder.docker.docker.build_image")
    @patch("builder.docker.docker.is_on_tagged_commit")
    def test_build_docker_private_repo(
        self, mock_is_on_tagged_commit, mock_build_image, mock_upload_image
    ):
        args = Namespace(
            command="docker",
            no_tagged_commit=False,
            tag="foo",
            no_tag_latest=False,
            secret="secret",
            no_upload=False,
            username="username",
            repo_name="repo_name",
            private_repo="private.repo.com",
        )

        build_docker(args)

        mock_is_on_tagged_commit.assert_called_once()
        mock_build_image.assert_called_once_with(
            tag="foo", repository="private.repo.com", no_tag_latest=False
        )
        mock_upload_image.assert_called_once_with(
            username="username",
            image_name="private.repo.com/username/repo_name",
            secret="secret",
            registry="private.repo.com",
        )

    @patch("builder.docker.docker.upload_image")
    @patch("builder.docker.docker.build_image")
    @patch("builder.docker.docker.is_on_tagged_commit")
    def test_build_docker_no_upload(
        self, mock_is_on_tagged_commit, mock_build_image, mock_upload_image
    ):
        args = Namespace(
            command="docker",
            no_tagged_commit=False,
            tag="foo",
            no_tag_latest=False,
            secret="secret",
            no_upload=True,
            username="username",
            repo_name="repo_name",
            private_repo=None,
        )

        build_docker(args)

        mock_is_on_tagged_commit.assert_called_once()
        mock_build_image.assert_called_once_with(
            tag="foo", repository="https://index.docker.io/v1/", no_tag_latest=False
        )
        mock_upload_image.assert_not_called()


if __name__ == "__main__":
    unittest.main()
