import unittest
from unittest.mock import patch, Mock, call
from docker.errors import BuildError, DockerException  # type: ignore[import-untyped]
import sys
import os
from contextlib import contextmanager
from builder.docker.docker import build_image


# Context manager to suppress stdout
@contextmanager
def suppress_stdout():
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:
            yield
        finally:
            sys.stdout = old_stdout


class TestBuildImage(unittest.TestCase):
    @patch("builder.docker.docker.docker")
    @patch("builder.docker.docker.logger")
    @patch("builder.docker.docker.tag_image")
    def test_build_image_success(self, mock_tag_image, mock_logger, mock_docker):
        client_mock = Mock()
        image_mock = Mock()
        image_mock.id = "test_image_id"
        logs_mock = [
            {"stream": "Step 1/2 : FROM python:3.8"},
            {"stream": "Step 2/2 : CMD ['python', 'app.py']"},
        ]
        client_mock.images.build.return_value = (image_mock, logs_mock)
        mock_docker.from_env.return_value = client_mock

        with suppress_stdout():
            result = build_image(
                tag="test:tag", no_tag_latest=False, repository="test_repo"
            )

        self.assertEqual(result, image_mock)
        client_mock.images.build.assert_called_once_with(path=".", tag="test:tag")
        mock_logger.info.assert_has_calls(
            [
                call("Building image with tag: test:tag"),
                call("Successfully built image: test_image_id"),
                call("Tagging image latest"),
            ]
        )
        client_mock.close.assert_called_once()
        mock_tag_image.assert_called_once_with(
            image=image_mock, repository="test_repo", new_tag="latest"
        )

    @patch("builder.docker.docker.docker")
    @patch("builder.docker.docker.logger")
    @patch("builder.docker.docker.tag_image")
    def test_build_image_success_no_latest_tag(
        self, mock_tag_image, mock_logger, mock_docker
    ):
        client_mock = Mock()
        image_mock = Mock()
        image_mock.id = "test_image_id"
        logs_mock = [
            {"stream": "Step 1/2 : FROM python:3.8"},
            {"stream": "Step 2/2 : CMD ['python', 'app.py']"},
        ]
        client_mock.images.build.return_value = (image_mock, logs_mock)
        mock_docker.from_env.return_value = client_mock

        with suppress_stdout():
            result = build_image(
                tag="test:tag", no_tag_latest=True, repository="test_repo"
            )

        self.assertEqual(result, image_mock)
        client_mock.images.build.assert_called_once_with(path=".", tag="test:tag")
        mock_logger.info.assert_has_calls(
            [
                call("Building image with tag: test:tag"),
                call("Successfully built image: test_image_id"),
            ]
        )
        client_mock.close.assert_called_once()
        mock_tag_image.assert_not_called()

    @patch("builder.docker.docker.docker")
    @patch("builder.docker.docker.logger")
    @patch("builder.docker.docker.tag_image")
    def test_build_image_build_error(self, mock_tag_image, mock_logger, mock_docker):
        client_mock = Mock()
        mock_docker.from_env.return_value = client_mock
        client_mock.images.build.side_effect = BuildError("Build failed", build_log="")

        with suppress_stdout():
            with self.assertRaises(BuildError):
                build_image(tag="test:tag", no_tag_latest=False, repository="test_repo")

        client_mock.images.build.assert_called_once_with(path=".", tag="test:tag")
        mock_logger.error.assert_called_once_with("Build failed")
        client_mock.close.assert_called_once()
        mock_tag_image.assert_not_called()

    @patch("builder.docker.docker.docker")
    @patch("builder.docker.docker.logger")
    @patch("builder.docker.docker.tag_image")
    def test_build_image_docker_exception(
        self, mock_tag_image, mock_logger, mock_docker
    ):
        client_mock = Mock()
        mock_docker.from_env.return_value = client_mock
        client_mock.images.build.side_effect = DockerException("Docker error")

        with suppress_stdout():
            with self.assertRaises(DockerException):
                build_image(tag="test:tag", no_tag_latest=False, repository="test_repo")

        client_mock.images.build.assert_called_once_with(path=".", tag="test:tag")
        mock_logger.error.assert_called_once_with("Docker error")
        client_mock.close.assert_called_once()
        mock_tag_image.assert_not_called()


if __name__ == "__main__":
    unittest.main()
