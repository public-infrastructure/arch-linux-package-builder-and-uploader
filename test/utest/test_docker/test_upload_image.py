import unittest
from unittest.mock import patch, MagicMock
from docker.errors import DockerException  # type: ignore[import-untyped]
from builder.docker.docker import upload_image


class TestUploadImage(unittest.TestCase):

    @patch("docker.from_env")
    @patch("builder.docker.docker.logger")
    def test_upload_image_success(self, mock_logger, mock_from_env):
        # Setup mocks
        mock_client = MagicMock()
        mock_from_env.return_value = mock_client
        mock_response = {"status": "Image successfully pushed"}
        mock_client.images.push.return_value = mock_response

        # Call the function
        upload_image("username", "image_name", "secret", "registry")

        # Assertions
        mock_client.login.assert_called_once_with(
            username="username", password="secret", registry="registry"
        )
        mock_client.images.push.assert_called_once_with(
            "image_name", stream=True, decode=True
        )
        mock_logger.info.assert_called_once_with(mock_response)
        mock_client.close.assert_called_once()

    @patch("docker.from_env")
    @patch("builder.docker.docker.logger")
    def test_upload_image_login_failure(self, mock_logger, mock_from_env):
        # Setup mocks
        mock_client = MagicMock()
        mock_from_env.return_value = mock_client
        mock_client.login.side_effect = DockerException("Login failed")

        # Call the function and expect an exception
        with self.assertRaises(DockerException) as cm:
            upload_image("username", "image_name", "secret", "registry")

        self.assertEqual(str(cm.exception), "Login failed")
        mock_client.close.assert_called_once()

    @patch("docker.from_env")
    @patch("builder.docker.docker.logger")
    def test_upload_image_push_failure(self, mock_logger, mock_from_env):
        # Setup mocks
        mock_client = MagicMock()
        mock_from_env.return_value = mock_client
        mock_client.login.return_value = None
        mock_client.images.push.side_effect = DockerException("Push failed")

        # Call the function and expect an exception
        with self.assertRaises(DockerException) as cm:
            upload_image("username", "image_name", "secret", "registry")

        self.assertEqual(str(cm.exception), "Push failed")
        mock_client.close.assert_called_once()

    @patch("docker.from_env")
    @patch("builder.docker.docker.logger")
    def test_upload_image_exception_handling(self, mock_logger, mock_from_env):
        # Setup mocks
        mock_client = MagicMock()
        mock_from_env.return_value = mock_client
        mock_client.login.side_effect = DockerException("Login failed")

        # Call the function and expect an exception
        with self.assertRaises(DockerException) as cm:
            upload_image("username", "image_name", "secret", "registry")

        self.assertEqual(str(cm.exception), "Login failed")
        mock_client.close.assert_called_once()


if __name__ == "__main__":
    unittest.main()
