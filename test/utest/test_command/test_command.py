import unittest
from unittest.mock import patch, MagicMock
import subprocess
from builder.command import run_shell_command


class TestRunShellCommand(unittest.TestCase):

    @patch("builder.command.command.subprocess.run")
    def test_run_shell_command_success(self, mock_run):
        # Setup mocks
        mock_result = MagicMock()
        mock_result.returncode = 0
        mock_result.stdout = "Command succeeded"
        mock_result.stderr = ""
        mock_run.return_value = mock_result

        command = ["echo", "Hello World"]
        result = run_shell_command(command)

        # Verify results
        self.assertEqual(result.code, 0)
        self.assertEqual(result.stdout, "Command succeeded")
        self.assertEqual(result.stderr, "")

    @patch("builder.command.command.subprocess.run")
    @patch("builder.command.command.logger.info")
    def test_run_shell_command_failure(self, mock_logger, mock_run):
        # Setup mocks
        mock_result = MagicMock()
        mock_result.returncode = 1
        mock_result.stdout = ""
        mock_result.stderr = "Command failed"
        mock_run.side_effect = subprocess.CalledProcessError(
            1, "cmd", output="", stderr="Command failed"
        )

        command = ["false"]  # A command that fails
        with self.assertRaises(subprocess.CalledProcessError):
            run_shell_command(command)

        # Check logger calls
        expected_calls = [
            unittest.mock.call("Error occurred while running command: Command failed"),
            unittest.mock.call(subprocess.CalledProcessError(1, "cmd")),
        ]

        # self.assertEqual(mock_logger.call_args_list, expected_calls)
        self.assertEqual(mock_logger.call_args_list[0], expected_calls[0])
        self.assertEqual(len(mock_logger.call_args_list), 2)


if __name__ == "__main__":
    unittest.main()
