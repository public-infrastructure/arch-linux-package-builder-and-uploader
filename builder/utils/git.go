package utils

import (
	"fmt"
	"strings"

	"github.com/rs/zerolog/log"
)

// isOnTaggedCommit enforces that we are on a tagged commit in the Git repository.
func IsOnTaggedCommit() (string, error) {
	log.Info().Msg("Enforcing that we are on a tagged branch...")

	// Get the current commit ID
	currentCommitID, stderr, err := RunCommand([]string{"git", "rev-parse", "HEAD"})
	if err != nil {
		log.Error().Str("stdout", currentCommitID).Str("stderr", stderr).Msg("failed to get current commit ID")
		return "", fmt.Errorf("failed to get current commit ID: %w", err)
	}

	taggedCommit := false
	var tags string
	var tag string

	// Get the list of tags
	gitTags, stderr, err := RunCommand([]string{"git", "tag"})
	if err != nil {
		log.Error().Str("stdout", gitTags).Str("stderr", stderr).Msg("failed to fetch Git tags")
		return "", fmt.Errorf("failed to fetch Git tags: %w", err)
	}

	log.Debug().Str("gitTags", gitTags).Str("our commit hash", currentCommitID).Msg("Our git tags we're looking in for our tag")
	for _, gitTag := range strings.Split(gitTags, "\n") {
		log.Debug().Str("tag", gitTag).Msg("Looking at this tag now")
		if gitTag == "" {
			continue
		}
		tagCommitID, stderr, err := RunCommand([]string{"git", "rev-list", "-n", "1", gitTag})
		if err != nil {
			log.Error().Err(err).Str("current tag", gitTag).Str("current Tag", tagCommitID).Str("stderr", stderr).Msg("failed to get commit ID for tag")
			return "", fmt.Errorf("failed to get commit ID for tag %s: %w", gitTag, err)
		}

		tags += fmt.Sprintf("\n%s    ::::    %s", gitTag, tagCommitID)
		if tagCommitID == currentCommitID {
			taggedCommit = true
			tag = gitTag
		}
	}

	if !taggedCommit {
		log.Info().Msgf(
			"\nThere are no tags associated with the current commit hash.\nWe will not be uploading this build so we will not build it...\nTags and their commit IDs:\n\nCurrent commit:   %s\n",
			currentCommitID,
		)
		log.Info().Msg(
			"No tag found. If you're not building on a tagged commit you MUST specify --no-tagged-commit --branch {BRANCH_NAME}",
		)
		return "", fmt.Errorf("no tagged commit found")
	}

	log.Info().Msgf("Tagged commit: %s", tag)
	return tag, nil
}
