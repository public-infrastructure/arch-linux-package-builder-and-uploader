package utils

import (
	"bytes"
	"os/exec"
)

// RunCommand executes a shell command and returns stdout, stderr, and an error (if any).
func RunCommand(command []string) (string, string, error) {
	if len(command) == 0 {
		return "", "", nil
	}

	cmd := exec.Command(command[0], command[1:]...) // First element is the command, rest are args

	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	err := cmd.Run()
	return stdout.String(), stderr.String(), err
}
