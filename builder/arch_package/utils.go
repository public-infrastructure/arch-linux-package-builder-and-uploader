package archpackage

import (
	"encoding/base64"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"sort"
	"strings"

	"github.com/rs/zerolog/log"
	"golang.org/x/net/html"
)

// extractVersion searches for a version pattern (e.g., v1.2.3) in the filename and returns it.
func extractVersion(filename string) (string, error) {
	// Regular expression pattern to match version information (e.g., v1.2.3)
	versionPattern := regexp.MustCompile(`v\d+\.\d+\.\d+`)

	// Find the first match in the filename
	match := versionPattern.FindString(filename)

	// Return an error if no match is found
	if match == "" {
		return "", errors.New("no version found in filename")
	}

	return match, nil
}

// createGPGKey imports a GPG key from the given base64-encoded signing secret.
func createGPGKey(signingSecret string) error {
	fmt.Println("Setting up the signing secret...")

	// Decode the base64 signing secret
	decodedSecret, err := base64.StdEncoding.DecodeString(signingSecret)
	if err != nil {
		return fmt.Errorf("failed to decode signing secret: %w", err)
	}

	// Temporary file path
	tmpFilePath := "/tmp/private.pgp"

	// Write the decoded secret to a temporary file
	if err := os.WriteFile(tmpFilePath, decodedSecret, 0600); err != nil {
		return fmt.Errorf("failed to write private key file: %w", err)
	}

	// Ensure cleanup of the temporary file
	defer func() {
		if err := os.Remove(tmpFilePath); err != nil && !errors.Is(err, os.ErrNotExist) {
			fmt.Printf("warning: failed to remove temp file %s: %v\n", tmpFilePath, err)
		}
	}()

	// Import the GPG key
	cmd := exec.Command("gpg", "--import", tmpFilePath)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return fmt.Errorf("failed to import GPG key: %w", err)
	}

	return nil
}

// updatePkgver updates the pkgver value in the given file.
func updatePkgver(filePath, tag string) error {
	fmt.Printf("Updating our package version to %s in the file %s\n", tag, filePath)

	// Read the file contents
	contents, err := os.ReadFile(filePath)
	if err != nil {
		return fmt.Errorf("failed to read file: %w", err)
	}

	// Convert to string
	contentStr := string(contents)

	// Regex pattern to match 'pkgver=' line
	re := regexp.MustCompile(`(?m)^pkgver\s*=\s*(\S+)$`)

	// Find the existing pkgver value
	matches := re.FindStringSubmatch(contentStr)
	if len(matches) < 2 {
		return fmt.Errorf("the line containing 'pkgver=' was not found in the file")
	}

	currentPkgver := matches[1]

	// If the current pkgver is already the desired tag, return early
	if currentPkgver == tag {
		log.Debug().Msg("Package version is already up to date. No changes needed.")
		return nil
	}

	// Replace pkgver value using regex
	newContents := re.ReplaceAllString(contentStr, fmt.Sprintf("pkgver=%s", tag))

	// Write the modified contents back to the file
	if err := os.WriteFile(filePath, []byte(newContents), 0644); err != nil {
		return fmt.Errorf("failed to write updated file: %w", err)
	}

	fmt.Println("Package version updated successfully.")
	return nil
}

// getRemotePackages fetches and extracts package filenames from a remote mirror page.
func getRemotePackages(baseMirrorURL, packageName string) ([]string, error) {
	// Send an HTTP GET request
	resp, err := http.Get(baseMirrorURL)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch URL: %w", err)
	}
	defer resp.Body.Close()

	// Check for HTTP errors
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("HTTP request failed with status: %d", resp.StatusCode)
	}

	// Parse the HTML content
	var files []string
	tokenizer := html.NewTokenizer(resp.Body)

	for {
		tokenType := tokenizer.Next()
		switch tokenType {
		case html.ErrorToken: // End of document
			return files, nil
		case html.StartTagToken:
			tagName, _ := tokenizer.TagName()
			if string(tagName) == "a" {
				// Extract href attribute
				for {
					attrKey, attrVal, moreAttr := tokenizer.TagAttr()
					if string(attrKey) == "href" {
						href := string(attrVal)
						if strings.HasSuffix(href, ".pkg.tar.zst") && strings.Contains(href, packageName) {
							files = append(files, href)
						}
					}
					if !moreAttr {
						break
					}
				}
			}
		}
	}
}

// getSortedVersions extracts version numbers from a list of package names and returns a sorted list.
func getSortedVersions(packages []string) ([]string, error) {
	var versions []string

	// Extract versions from package names
	for _, pkg := range packages {
		version, err := extractVersion(pkg)
		if err != nil {
			// Log the error, but continue processing other packages
			fmt.Printf("Skipping package %s: %s\n", pkg, err)
			continue
		}
		versions = append(versions, version)
	}

	// Remove duplicates by converting to a map and then back to a slice
	uniqueVersions := make(map[string]struct{})
	for _, version := range versions {
		uniqueVersions[version] = struct{}{}
	}

	// Convert map keys to slice
	var uniqueVersionSlice []string
	for version := range uniqueVersions {
		uniqueVersionSlice = append(uniqueVersionSlice, version)
	}

	// Sort the versions in descending order
	sort.Sort(sort.Reverse(sort.StringSlice(uniqueVersionSlice)))

	return uniqueVersionSlice, nil
}

// getRemoteVersions extracts version numbers from a list of package names and returns the list.
func getRemoteVersions(packages []string) ([]string, error) {
	var versions []string

	// Extract versions from package names
	for _, pkg := range packages {
		version, err := extractVersion(pkg)
		if err != nil {
			// Log the error, but continue processing other packages
			fmt.Printf("Skipping package %s: %s\n", pkg, err)
			continue
		}
		versions = append(versions, version)
	}

	return versions, nil
}
