package archpackage

import (
	"builder/args"
	"builder/utils"
	"bytes"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"strings"

	"github.com/rs/zerolog/log"
)

// verifyRemotePackage fetches remote packages, checks versions, and compares with the local tag to verify the build process.
func verifyRemotePackage(baseMirrorURL, packageName, tag string) error {
	// Fetch list of packages from the remote repository
	packages, err := getRemotePackages(baseMirrorURL, packageName)
	if err != nil {
		return fmt.Errorf("failed to fetch remote packages: %w", err)
	}

	// If there are no remote packages, we're guaranteed to build the latest version
	if len(packages) < 1 {
		return nil
	}

	// Get a list of versions
	versions, err := getRemoteVersions(packages)
	if err != nil {
		return fmt.Errorf("failed to get remote versions: %w", err)
	}

	// Sort the versions and add the local tag to check if it's the latest
	sortedTags, err := getSortedVersions(append(packages, tag))
	if err != nil {
		return fmt.Errorf("failed to sort remote versions: %w", err)
	}

	// Get the latest tag
	latestTag := sortedTags[0]

	// If the local tag is already uploaded, or if the local tag isn't the latest, don't upload
	if tag != latestTag || contains(versions, tag) {
		errorStr := fmt.Sprintf(`
Current tag is not newer than deployed tag...

We do NOT support automatically building and deploying silent patches!

All new builds MUST be a newer version than what's on the mirror.
We also don't support -X releases. For example v0.3.0-1 to v0.3.0-2
Increment it to v0.3.1

Debug info:

local git tag: %s
latest remote tag: %s

raw unsorted versions:
%v

sorting them:
%v

head:
%s
`, tag, latestTag, versions, sortedTags, sortedTags[0])

		// Log the error message
		log.Info().Msg(errorStr)

		// Return an error with the message
		return fmt.Errorf("cannot build due to version issue. See prior output")
	}

	return nil
}

// contains checks if a string exists in a slice of strings.
func contains(slice []string, str string) bool {
	for _, s := range slice {
		if s == str {
			return true
		}
	}
	return false
}

// getBuiltFiles detects newly built package and signature files.
func getBuiltFiles(beforeFiles []string, signed bool, packageName string) (string, *string, error) {
	log.Info().Msg("Detecting files that were built...")

	// Get list of files after the build
	afterFiles, err := os.ReadDir(".")
	if err != nil {
		return "", nil, fmt.Errorf("failed to list directory: %w", err)
	}

	// Convert ReadDir output to file names
	afterFileNames := make(map[string]struct{})
	for _, file := range afterFiles {
		afterFileNames[file.Name()] = struct{}{}
	}

	// Find new files
	newFiles := []string{}
	for file := range afterFileNames {
		found := false
		for _, before := range beforeFiles {
			if file == before {
				found = true
				break
			}
		}
		if !found {
			newFiles = append(newFiles, file)
		}
	}

	// Filter files based on package name and extension
	var filteredFiles []string
	for _, filename := range newFiles {
		if strings.Contains(filename, packageName) {
			if strings.HasSuffix(filename, ".pkg.tar.zst") || strings.HasSuffix(filename, ".sig") {
				filteredFiles = append(filteredFiles, filename)
			} else {
				log.Info().Msgf("Discarding %s since it's not a .pkg or .sig", filename)
			}
		} else {
			log.Info().Msgf("Discarding %s since it doesn't include the package name %s", filename, packageName)
		}
	}

	log.Info().Msg("The files that remain in our list of files are:")
	log.Info().Strs("filtered_files", filteredFiles).Send()

	// Validation checks
	if signed && len(filteredFiles) > 2 {
		log.Error().Strs("filtered_files", filteredFiles).Msg("Too many files detected")
		return "", nil, fmt.Errorf("we have too many files... %d", len(filteredFiles))
	} else if !signed && len(filteredFiles) > 1 {
		log.Error().Strs("filtered_files", filteredFiles).Msg("Too many files detected")
		return "", nil, fmt.Errorf("we have too many files... %d", len(filteredFiles))
	}

	// Process the files
	var packageFile string
	var signatureFile *string

	for _, file := range filteredFiles {
		if strings.Contains(file, "pkg") && strings.Contains(file, packageName) {
			if strings.Contains(file, "sig") {
				signatureFile = &file
			} else {
				packageFile = file
			}
		}
	}

	if packageFile == "" {
		return "", nil, errors.New("failed to detect built package")
	}

	if signed && signatureFile == nil {
		return "", nil, errors.New("failed to detect built package signature")
	}

	return packageFile, signatureFile, nil
}

// uploadPackage uploads a package and an optional signature file in a single multipart request.
func uploadPackage(signaturePath *string, packagePath, packageName, uploadSecret, uploadURL string) error {
	log.Debug().Msg("Uploading package...")
	if uploadSecret == "" {
		return fmt.Errorf("upload secret is an empty string")
	}

	// Open the package file
	packageFile, err := os.Open(packagePath)
	if err != nil {
		return fmt.Errorf("failed to open package file: %w", err)
	}
	defer packageFile.Close()

	// Create a buffer to store multipart form data
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	// Add the package file
	packagePart, err := writer.CreateFormFile("package", packagePath)
	if err != nil {
		return fmt.Errorf("failed to create form file for package: %w", err)
	}
	if _, err = io.Copy(packagePart, packageFile); err != nil {
		return fmt.Errorf("failed to copy package file: %w", err)
	}

	// If a signature is provided, add it
	if signaturePath != nil {
		signatureFile, err := os.Open(*signaturePath)
		if err != nil {
			return fmt.Errorf("failed to open signature file: %w", err)
		}
		defer signatureFile.Close()

		signaturePart, err := writer.CreateFormFile("signature", *signaturePath)
		if err != nil {
			return fmt.Errorf("failed to create form file for signature: %w", err)
		}
		if _, err = io.Copy(signaturePart, signatureFile); err != nil {
			return fmt.Errorf("failed to copy signature file: %w", err)
		}
	}

	// Close the writer to finalize the multipart form data
	if err := writer.Close(); err != nil {
		return fmt.Errorf("failed to close multipart writer: %w", err)
	}

	// Create the request
	packageURL := fmt.Sprintf("%s/package/arch", uploadURL)
	req, err := http.NewRequest("PUT", packageURL, body)
	if err != nil {
		return fmt.Errorf("failed to create upload request: %w", err)
	}

	// Set headers
	req.Header.Set("Content-Type", writer.FormDataContentType())
	req.Header.Set("X-MutableMango-Upload-Token", uploadSecret)
	req.Header.Set("project", packageName)

	// Perform the request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("failed to upload package: %w", err)
	}
	defer resp.Body.Close()

	// Check for HTTP errors
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("HTTP status code for uploading package is %d", resp.StatusCode)
	}

	log.Debug().Msg("Upload successful!")
	return nil
}

func BuildArchPackage(args *args.ArchBuildArgs) error {
	var tag string
	// Use the provided branch if we shouldn't use the current tag and enforce it
	if args.NoTaggedCommit {
		tag = args.Branch
	} else {
		branch, err := utils.IsOnTaggedCommit()
		if err != nil {
			log.Error().Err(err).Msg("problem getting tagged commit")
			return err
		}
		tag = branch
	}

	// If we're uploading then we cannot upload a package that is older or equal to the version on the remote mirror
	if !args.NoUpload {
		err := verifyRemotePackage(args.BaseMirrorURL, args.PackageName, tag)
		if err != nil {
			log.Error().Err(err).Msg("Unknown error verifying remote packages")
			return err
		}
	}

	// Import our GPG key if we're supposed to sign the package
	if args.Sign {
		createGPGKey(args.GPGKey)
	}

	// Update the version in the package build so we can get the right one in the name
	if !args.NoUpdatePkgbuildVersion {
		err := updatePkgver("PKGBUILD", tag)
		if err != nil {
			log.Error().Err(err).Msg("Unknown Error updating tag in PKGBUILD")
			return err
		}
	}

	// Get the files in our directory BEFORE we build our package
	files, err := os.ReadDir(".")
	if err != nil {
		return fmt.Errorf("failed to read directory: %w", err)
	}
	var beforeFiles []string
	for _, file := range files {
		beforeFiles = append(beforeFiles, file.Name())
	}

	// Makepkg command
	makepkgCommand := []string{"/usr/bin/makepkg"}

	// Check if we're supposed to sign
	if args.Sign {
		makepkgCommand = append(makepkgCommand, "--sign")
	}

	log.Debug().Interface("makepkg command", makepkgCommand).Msg("Making package with the following command")
	stdout, stderr, err := utils.RunCommand(makepkgCommand)
	if err != nil {
		log.Error().Err(err).Str("stderr", stderr).Msg("Unknown error making package")
		return err
	}
	log.Debug().Str("stdout", stdout).Msg("Successful makepkg run")

	var packageName string
	var signatureName *string
	if !args.NoUpload || !args.TestBuild {
		packageName, signatureName, err = getBuiltFiles(beforeFiles, args.Sign, args.PackageName)
		if err != nil {
			log.Error().Err(err).Msg("unknown error getting built files")
			return err
		}

		// TODO upload
		err = uploadPackage(signatureName, packageName, args.PackageName, args.Secret, args.UploadURL)
		if err != nil {
			log.Error().Err(err).Msg("unknown error uploading files")
			return err
		}
	}

	return nil
}
