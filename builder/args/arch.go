package args

import (
	"flag"
	"fmt"
	"log"
	"os"
)

// ArchBuildArgs holds parsed arguments for arch package builds
type ArchBuildArgs struct {
	LogLevel                string
	Secret                  string
	GPGKey                  string
	Sign                    bool
	NoUpload                bool
	UploadURL               string
	PackageName             string
	BaseMirrorURL           string
	NoTaggedCommit          bool
	Branch                  string
	TestBuild               bool
	NoUpdatePkgbuildVersion bool
}

// parseArchBuildArgs parses and validates command-line arguments for arch package builds
func parseArchBuildArgs(args []string) (*ArchBuildArgs, error) {
	fs := flag.NewFlagSet("arch_package", flag.ExitOnError)

	var opts ArchBuildArgs
	fs.StringVar(&opts.LogLevel, "log-level", "info", "The logging level (notset, debug, info, warning, error, critical)")
	fs.StringVar(&opts.Secret, "secret", "", "Secret required to authenticate with uploader")
	fs.StringVar(&opts.GPGKey, "gpg-key", "", "Base64-encoded GPG key for signing")
	fs.BoolVar(&opts.Sign, "sign", false, "Sign the package (requires --gpg-key)")
	fs.BoolVar(&opts.NoUpload, "no-upload", false, "Don't upload to the repository")
	fs.StringVar(&opts.UploadURL, "upload-url", "", "Arch Linux Package Uploader URL")
	fs.StringVar(&opts.PackageName, "package-name", "", "Name of the package (e.g., bash-utilities)")
	fs.StringVar(&opts.BaseMirrorURL, "base-mirror-url", "", "URL of the package mirror")
	fs.BoolVar(&opts.NoTaggedCommit, "no-tagged-commit", false, "Allow builds from non-tagged commits")
	fs.StringVar(&opts.Branch, "branch", "", "Branch to use for the build")
	fs.BoolVar(&opts.TestBuild, "test-build", false, "Perform a test build (requires --branch)")
	fs.BoolVar(&opts.NoUpdatePkgbuildVersion, "no-update-pkgbuild-version", false, "Do not override PKGBUILD version")

	// Parse the command-line arguments
	err := fs.Parse(args)
	if err != nil {
		return nil, err
	}

	// Validation logic
	if opts.Sign && opts.GPGKey == "" {
		return nil, fmt.Errorf("--sign requires --gpg-key to be specified")
	}

	if opts.Branch != "" && !opts.NoTaggedCommit && !opts.TestBuild {
		return nil, fmt.Errorf("--branch requires --no-tagged-commit or --test-build to be specified")
	}

	if opts.TestBuild && opts.Branch == "" {
		return nil, fmt.Errorf("--test-build requires --branch to be specified")
	}

	if opts.TestBuild && !opts.NoUpload {
		log.Println("Setting --no-upload to true since we were called with --test-build")
		opts.NoUpload = true
	}

	if opts.TestBuild && !opts.NoTaggedCommit {
		log.Println("Setting --no-tagged-commit to true since we were called with --test-build")
		opts.NoTaggedCommit = true
	}

	if !opts.NoUpload {
		if opts.UploadURL == "" {
			return nil, fmt.Errorf("--upload-url is required to upload a package")
		}
		if opts.PackageName == "" {
			return nil, fmt.Errorf("--package-name is required to upload a package")
		}
		if opts.BaseMirrorURL == "" {
			return nil, fmt.Errorf("--base-mirror-url is required to upload a package")
		}
		if opts.Secret == "" {
			return nil, fmt.Errorf("--secret is required to upload a package")
		}
	}

	return &opts, nil
}

// handleArchPackage handles the "arch_package" subcommand
func handleArchPackage(args []string) {
	opts, err := parseArchBuildArgs(args)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}

	fmt.Println("Executing Arch Package Build with:", opts)
}
