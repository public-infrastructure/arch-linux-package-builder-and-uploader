package args

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

// DockerBuildArgs holds parsed arguments for Docker builds
type DockerBuildArgs struct {
	LogLevel       string
	NoTaggedCommit bool
	Tags           []string
	Secret         string
	NoUpload       bool
	Username       string
	RepoName       string
	RepoURL        string
	BuildArgs      map[string]string
}

// Custom type to handle multiple --build-arg values
type buildArgFlag map[string]string

func (b *buildArgFlag) String() string {
	if b == nil {
		return ""
	}
	var args []string
	for key, value := range *b {
		args = append(args, fmt.Sprintf("%s=%s", key, value))
	}
	return strings.Join(args, ", ")
}

func (b *buildArgFlag) Set(value string) error {
	// Ensure the map is initialized
	if *b == nil {
		*b = make(map[string]string)
	}

	// Parse key=value format
	parts := strings.SplitN(value, "=", 2)
	if len(parts) != 2 {
		return fmt.Errorf("invalid format for --build-arg: %s (expected KEY=VALUE)", value)
	}

	key, val := parts[0], parts[1]
	(*b)[key] = val
	return nil
}

// Custom type for handling multiple --tag arguments
type multiStringFlag struct {
	values []string
}

func (m *multiStringFlag) String() string {
	return strings.Join(m.values, ", ")
}

func (m *multiStringFlag) Set(value string) error {
	m.values = append(m.values, value)
	return nil
}

// parseDockerBuildArgs parses and validates command-line arguments for Docker builds
func parseDockerBuildArgs(args []string) (*DockerBuildArgs, error) {
	fs := flag.NewFlagSet("docker", flag.ExitOnError)
	var tags multiStringFlag   // Custom flag type to collect multiple values
	var buildArgs buildArgFlag // Custom flag type for multiple --build-arg flags

	var opts DockerBuildArgs
	fs.StringVar(&opts.LogLevel, "log-level", "info", "The logging level (notset, debug, info, warning, error, critical)")
	fs.BoolVar(&opts.NoTaggedCommit, "no-tagged-commit", false, "Allow builds from non-tagged commits")
	fs.Var(&tags, "tag", "Specify tags (can be used multiple times) NOTE that ALL tags are pushed to registry repo")
	fs.Var(&buildArgs, "build-arg", "Specify build arguments (format: KEY=VALUE, can be used multiple times)")
	fs.StringVar(&opts.Secret, "secret", "", "Docker secret used to login to the registry")
	fs.BoolVar(&opts.NoUpload, "no-upload", false, "Don't upload to a registry")
	fs.StringVar(&opts.Username, "username", "", "Username for the Docker repository")
	fs.StringVar(&opts.RepoURL, "repo-url", "", "The name of the repository to upload to")
	fs.StringVar(&opts.RepoName, "repo-name", "", "The name of the repository to use for the build. This is usually <username>/<project> \n note that this is used in calculating tags so this MUST be accurate")

	// Parse the command-line arguments
	err := fs.Parse(args)
	if err != nil {
		return nil, err
	}

	// Assign some custom values
	opts.Tags = tags.values
	opts.BuildArgs = buildArgs

	// Do some verification
	if opts.RepoName == "" {
		return nil, fmt.Errorf("--repo-name is required to build and tag correctly")
	}

	if !opts.NoUpload {
		if opts.Username == "" {
			return nil, fmt.Errorf("--username is required if we're uploading")
		}
		if opts.Secret == "" {
			return nil, fmt.Errorf("--secret is required if we're uploading")
		}
	}

	return &opts, nil
}

// handleDocker handles the "docker" subcommand
func handleDocker(args []string) {
	opts, err := parseDockerBuildArgs(args)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}

	fmt.Println("Executing Docker Build with:", opts)
}
