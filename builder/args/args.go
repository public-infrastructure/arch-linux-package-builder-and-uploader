package args

import (
	"errors"
	"fmt"
	"os"
)

// Command represents the available subcommands
type Command string

const (
	ARCH   Command = "arch_package"
	DOCKER Command = "docker"
)

// GetArgs parses command-line arguments and returns the appropriate struct
func GetArgs(args []string) (Command, interface{}, error) {
	if len(args) < 2 {
		return "", nil, errors.New("no subcommand provided (expected 'arch_package' or 'docker')")
	}

	subcommand := Command(args[1])
	switch subcommand {
	case ARCH:
		opts, err := parseArchBuildArgs(args[2:])
		return subcommand, opts, err
	case DOCKER:
		opts, err := parseDockerBuildArgs(args[2:])
		return subcommand, opts, err
	default:
		return "", nil, fmt.Errorf("unknown subcommand: %s", args[1])
	}
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Expected 'arch_package' or 'docker' subcommand")
		os.Exit(1)
	}

	subcommand := os.Args[1] // Get the subcommand
	switch subcommand {
	case string(ARCH):
		handleArchPackage(os.Args[2:])
	case string(DOCKER):
		handleDocker(os.Args[2:])
	default:
		fmt.Printf("Unknown subcommand: %s\n", subcommand)
		os.Exit(1)
	}
}
