package docker

import (
	"archive/tar"
	"builder/args"
	"builder/utils"
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"regexp"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/image"
	"github.com/docker/docker/api/types/registry"
	"github.com/docker/docker/client"
	"github.com/rs/zerolog/log"
)

// uploadImage logs into a Docker registry and pushes the specified image.
func uploadImage(cli *client.Client, username, imageName, secret, registryURL string, tags []string) error {
	ctx := context.Background()

	// Authenticate with Docker registry
	authConfig := registry.AuthConfig{
		Username:      username,
		Password:      secret,
		ServerAddress: registryURL,
	}

	authResponse, err := cli.RegistryLogin(ctx, authConfig)
	if err != nil {
		return fmt.Errorf("failed to authenticate with Docker registry: %w", err)
	}
	log.Debug().Interface("response", authResponse).Msg("Login successful: " + authResponse.Status)

	//// Push the image
	//pushResponse, err := cli.ImagePush(ctx, imageName, image.PushOptions{
	//	RegistryAuth: encodeAuthToBase64(authConfig),
	//})
	//if err != nil {
	//	return fmt.Errorf("failed to push image: %w", err)
	//}
	//defer pushResponse.Close()

	//// Print push response
	//if _, err := io.Copy(os.Stdout, pushResponse); err != nil {
	//	return fmt.Errorf("failed to read push response: %w", err)
	//}
	// Push each tag
	for _, tag := range tags {
		log.Info().Str("tag", tag).Msg("Pushing image tag")

		pushResponse, err := cli.ImagePush(ctx, tag, image.PushOptions{
			RegistryAuth: encodeAuthToBase64(authConfig),
		})
		if err != nil {
			return fmt.Errorf("failed to push image %s: %w", tag, err)
		}
		defer pushResponse.Close()

		// Print push response
		if _, err := io.Copy(os.Stdout, pushResponse); err != nil {
			return fmt.Errorf("failed to read push response: %w", err)
		}
	}

	fmt.Println("Image pushed successfully.")
	return nil
}

// Create a valid tar archive of the given directory
func createTarContext(dir string) (io.Reader, error) {
	buf := new(bytes.Buffer)
	tw := tar.NewWriter(buf)
	defer tw.Close()

	err := filepath.Walk(dir, func(file string, fi os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if fi.IsDir() {
			return nil
		}

		relPath, err := filepath.Rel(dir, file) // Relative path for tar header
		if err != nil {
			return err
		}

		f, err := os.Open(file)
		if err != nil {
			return err
		}
		defer f.Close()

		hdr, err := tar.FileInfoHeader(fi, "")
		if err != nil {
			return err
		}
		hdr.Name = relPath // Use relative path for correct tar structure

		if err := tw.WriteHeader(hdr); err != nil {
			return err
		}
		_, err = io.Copy(tw, f)
		return err
	})
	if err != nil {
		return nil, err
	}
	return bytes.NewReader(buf.Bytes()), nil
}

// cleanStream removes ANSI color codes and other escape sequences.
func cleanStream(input string) string {
	// Regex to match ANSI escape codes (e.g., color codes)
	re := regexp.MustCompile(`\x1b\[[0-9;]*[a-zA-Z]`)
	return re.ReplaceAllString(input, "")
}

// buildImage builds the Docker image with the specified tag.
func buildImage(cli *client.Client, tags []string, dockerfilePath string, buildArgs map[string]string) (string, error) {
	// Open the build context (Dockerfile directory or other file)
	buildContext, err := createTarContext(filepath.Dir(dockerfilePath))
	if err != nil {
		log.Error().Err(err).Msg("Failed to create build context")
		return "", fmt.Errorf("failed to create build context: %w", err)
	}

	// Convert map[string]string to map[string]*string
	buildArgsPtr := make(map[string]*string)
	for key, val := range buildArgs {
		valueCopy := val // Create a copy to ensure the pointer remains valid
		buildArgsPtr[key] = &valueCopy
	}

	// Set build options
	options := types.ImageBuildOptions{
		Tags:        tags,
		Remove:      true,
		ForceRemove: true,
		NoCache:     false,
		Dockerfile:  "Dockerfile",
		BuildArgs:   buildArgsPtr,
	}

	// Perform the image build
	buildResponse, err := cli.ImageBuild(context.Background(), buildContext, options)
	if err != nil {
		log.Error().Err(err).Msg("Unknown Error building image")
		return "", fmt.Errorf("failed to build image: %w", err)
	}
	log.Debug().Interface("response", buildResponse).Msg("build response")
	defer buildResponse.Body.Close()

	// Capture and process the logs
	var buildOutput bytes.Buffer
	decoder := json.NewDecoder(buildResponse.Body)

	// Loop through each log message
	for {
		var logEntry map[string]interface{}
		if err := decoder.Decode(&logEntry); err != nil {
			if err == io.EOF {
				break // End of stream
			}
			log.Error().Err(err).Msg("failed to read build log")
			return "", fmt.Errorf("failed to read build log: %w", err)
		}

		// Handle the log entry
		if stream, ok := logEntry["stream"].(string); ok {
			// Clean up the stream by removing color codes
			stream = cleanStream(stream)
			buildOutput.WriteString(stream)
		}

		// Handle errors (errorDetail)
		if errorDetail, ok := logEntry["errorDetail"].(map[string]interface{}); ok {
			errorMsg := fmt.Sprintf("Error: %v\n", errorDetail["message"])
			log.Error().Msg(errorMsg)
			return "", fmt.Errorf("build failed: %v", errorMsg)
		}
	}

	// Log our build output
	log.Debug().Str("Build Output", buildOutput.String()).Msg("Build STDOUT")

	// Process the build logs from the build response
	//_, err = stdcopy.StdCopy(os.Stdout, os.Stderr, buildResponse.Body)
	//if err != nil {
	//	log.Error().Err(err).Msg("Unknown Error processing build logs")
	//	return "", fmt.Errorf("failed to process build logs: %w", err)
	//}

	// Parse the logs to find the image ID
	//var imageID string
	//decoder := json.NewDecoder(buildResponse.Body)
	//for decoder.More() {
	//	var logEntry map[string]interface{}
	//	if err := decoder.Decode(&logEntry); err != nil {
	//		return "", err
	//	}
	//	if stream, ok := logEntry["stream"].(string); ok {
	//		// You may need to adjust this logic to find the actual image ID depending on the build output format
	//		if strings.Contains(stream, "Successfully built") {
	//			// Example: stream might contain: "Successfully built <image-id>"
	//			imageID = strings.TrimSpace(strings.TrimPrefix(stream, "Successfully built"))
	//			break
	//		}
	//	}
	//}

	//if imageID == "" {
	//	log.Error().Msg("Failed to find image ID in the build logs")
	//	return "", fmt.Errorf("failed to find image ID")
	//}

	log.Info().Interface("tags", tags).Msg("Successfully built image")
	//return imageID, nil
	return "", nil
}

// encodeAuthToBase64 encodes Docker registry credentials to Base64
func encodeAuthToBase64(authConfig registry.AuthConfig) string {
	authBytes, _ := json.Marshal(authConfig)
	return base64.StdEncoding.EncodeToString(authBytes)
}

func BuildDocker(args *args.DockerBuildArgs) error {
	if !args.NoTaggedCommit {
		_, err := utils.IsOnTaggedCommit()
		if err != nil {
			log.Error().Err(err).Msg("problem getting tagged commit")
			return err
		}
	}

	var repoURL string
	if args.RepoURL != "" {
		repoURL = args.RepoURL
	} else {
		repoURL = "https://index.docker.io/v1/"
	}
	// Create a list of tags
	var dockerTags []string
	for _, tag := range args.Tags {
		dockerTags = append(dockerTags, fmt.Sprintf("%s:%s", args.RepoName, tag))
	}
	log.Debug().Interface("user requested tags", args.Tags).Interface("dockerTags", dockerTags).Str("repository name", args.RepoName).Msg("Building container")

	// Create a client without specifying an API version
	//cli, err := client.NewClientWithOpts(client.FromEnv)
	//if err != nil {
	//	log.Error().Err(err).Msg("Failed to create Docker client")
	//}

	//// Get the server version
	//version, err := cli.ServerVersion(context.Background())
	//if err != nil {
	//	log.Error().Err(err).Msg("Failed to get Docker server version")
	//}

	// Recreate the client with the newest version of the API we can use
	cli, err := client.NewClientWithOpts(
		client.FromEnv, // Load Docker configuration from the environment
		//client.WithVersion(version.APIVersion),
		client.WithVersion("1.47"), // Force API version to 1.47
	)
	if err != nil {
		return fmt.Errorf("failed to create Docker client: %w", err)
	}
	defer cli.Close()

	imageID, err := buildImage(cli, dockerTags, "Dockerfile", args.BuildArgs)
	if err != nil {
		return fmt.Errorf("failed to build image: %w", err)
	}
	log.Debug().Str("image id", imageID).Msg("image id idk what i need this for")

	if !args.NoUpload {
		err := uploadImage(cli, args.Username, args.RepoName, args.Secret, repoURL, dockerTags)
		if err != nil {
			return fmt.Errorf("failed to upload image: %w", err)
		}
	}

	return nil
}
