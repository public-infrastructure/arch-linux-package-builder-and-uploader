package main

import (
	archpackage "builder/arch_package"
	"builder/args"
	"builder/docker"
	"fmt"
	"os"

	"github.com/rs/zerolog/log"
)

// main function demonstrating usage of GetArgs
func main() {
	subcommand, options, err := args.GetArgs(os.Args)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}

	switch subcommand {
	case args.ARCH:
		err := archpackage.BuildArchPackage(options.(*args.ArchBuildArgs))
		if err != nil {
			log.Error().Err(err).Msg("Unknown error building Arch Linux package")
			os.Exit(1)
		}
	case args.DOCKER:
		docker.BuildDocker(options.(*args.DockerBuildArgs))
		if err != nil {
			log.Error().Err(err).Msg("Unknown error building and uploading Docker image")
			os.Exit(1)
		}
	default:
		fmt.Println("Unknown command")
		os.Exit(1)
	}
}
